### STAGE 1: Build ###

# We label our stage as ‘node’
FROM node:8.16.0-alpine as node

# Create a directory where our app will be placed
RUN mkdir -p /usr/src/app

# Change directory so that our commands run inside this new directory
WORKDIR /usr/src/app

# Copy dependency definitions
COPY package*.json ./

# Install dependecies
RUN npm install

# Get all the code needed to run the app
COPY . .

# Run the angular in product
RUN npm run build -- --prod --output-path=dist

### STAGE 2: Setup ###

FROM nginx:1.16.0-alpine

## Copy our default nginx config
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

#copy dist content to html nginx folder, config nginx to point in index.html
COPY --from=node /usr/src/app/dist /usr/share/nginx/html

