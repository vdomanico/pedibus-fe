import * as auth from './reducers/auth.reducer';
import * as user from './reducers/user.reducer';
import * as line from './reducers/line.reducer';
import * as schedule from './reducers/schedule.reducer';
import * as userSchedule from './reducers/user.schedule.reducer';
import * as messageCount from './reducers/message.count.reducer';

export interface AppState {
  authState: auth.State;
  userState: user.State;
  lineState: line.State;
  scheduleState: schedule.State;
  userScheduleState: userSchedule.State;
  messageCountState: messageCount.State;
}
