import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {AppState} from '../app.states';
import {Router} from '@angular/router';
import {catchError, exhaustMap, map, mergeMap, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {getLines, LineActionTypes} from '../actions/line.actions';
import {LineService} from '../../services/line/line.service';
import {shareFailure, shareSuccess} from '../actions/rest.actions';
import {LineResource} from '../../model/LineResource';
import {LineSchedule} from '../../model/LineSchedule';

@Injectable()
export class LineEffects {

  constructor(
    private actions$: Actions,
    private lineService: LineService
  ) {}

  @Effect()
  lines$ = this.actions$.pipe(
    ofType(getLines),
    tap( () => console.log(LineActionTypes.GET_LINES + ' Effect - get lines') ),
    exhaustMap(() =>
      this.lineService.getLines().pipe(
        map( (lR: LineResource) =>  shareSuccess({payload: lR.lines})),
        catchError((error) => of(shareFailure(error)))
      )
    )
  );
}
