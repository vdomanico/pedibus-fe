import {Injectable} from '@angular/core';
import {ErrorDialogService} from '../../error/error-dialog.service';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {confirmAccountFailure, confirmAccountSuccess} from '../actions/auth.actions';
import {MatSnackBar} from '@angular/material';
import {map, tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class AccountEffects {

  constructor(
    private snackBar: MatSnackBar,
    private actions$: Actions,
    private router: Router,
  ) {}

  @Effect({dispatch: false})
  confirmationAccountOk$ = this.actions$.pipe(
    ofType(confirmAccountSuccess),
    tap(x => {
      this.snackBar.open('Hai confermato il tuo Account!', 'Accedi', {
        duration: 2000,
      }).afterDismissed().subscribe(value => this.router.navigate(['/login']));
    })
  );

  @Effect({dispatch: false})
  confirmationAccountKo$ = this.actions$.pipe(
    ofType(confirmAccountFailure),
    map(action => action.payload),
    tap(error => {
      if (error.status === 404 ) {
        this.router.navigate(['/login']);
        return;
      }
      this.snackBar.open('Attivazione account fallita', '', {
        duration: 4000,
      }).afterDismissed().subscribe(value => this.router.navigate(['/login']));
    })
  );
}
