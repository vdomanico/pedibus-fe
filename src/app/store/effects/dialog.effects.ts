import {Injectable} from '@angular/core';
import {ErrorDialogService} from '../../error/error-dialog.service';
import {Actions, createEffect, Effect, ofType} from '@ngrx/effects';
import {MatDialog} from '@angular/material';
import {map, mergeMap, switchMap, tap} from 'rxjs/operators';
import {openDialog} from '../actions/dialog.actions';
import {Store} from '@ngrx/store';
import {AppState} from '../app.states';
import {of} from 'rxjs';

@Injectable()
export class DialogEffects {
  constructor(
    private store: Store<AppState>,
    private errorDialogService: ErrorDialogService,
    private actions$: Actions,
    public dialog: MatDialog

  ) {}

  @Effect({dispatch: false})
  opened = this.actions$.pipe(
    ofType(openDialog),
    map (value => value.payload),
    tap( obj => {
      this.dialog.open(obj.component, obj.config);
    })
  );
}
