import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {catchError, exhaustMap, map, mergeMap, switchMap, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import {
  registerUser,
  UserActionTypes,
  registerUserFailure,
  registerUserSuccess,
  addRemoveUserGrant,
  shareSuccess,
  shareFailure, getUsers, openUserGrantsComponent, updateUser
} from '../actions/user.actions';
import {RegisterUser} from '../../model/RegisterUser';
import {UserService} from '../../services/user/user.service';
import {ErrorDialogService} from '../../error/error-dialog.service';
import {AddRemoveGrant} from '../../model/AddRemoveGrant';
import {UserManagement} from '../../model/UserManagement';
import {openDialog} from '../actions/dialog.actions';
import {UserGrantsComponent} from '../../components/user-grants/user-grants.component';
import {MatDialogConfig} from '@angular/material';
import {AppState} from '../app.states';
import {Store} from '@ngrx/store';
import {persistUser, storeUser} from '../actions/auth.actions';

@Injectable()
export class UserEffects {

  constructor(
    private errorDialogService: ErrorDialogService,
    private actions$: Actions,
    private userService: UserService,
    private store: Store<AppState>
  ) {}

  @Effect()
  register$ = this.actions$.pipe(
    ofType(registerUser),
    map(action => action.payload),
    tap( (user) => console.log(UserActionTypes.REGISTER_USER + ' Effect - register user' + user.email) ),
    exhaustMap((account: RegisterUser) =>
      this.userService.register(account).pipe(
        map(() => registerUserSuccess()),
        catchError((error: Error) => of(registerUserFailure({payload: error})))
      )
    )
  );

  @Effect({ dispatch: false })
  registerUserSuccess$ = this.actions$.pipe(
    ofType(registerUserSuccess),
    tap( () => console.log(UserActionTypes.REGISTER_USER_SUCCESS + ' Effect - register user success') ),
    tap(() => console.log('tell user ok') )); // TODO show message success

  @Effect({ dispatch: false })
  registerUserFailure$ = this.actions$.pipe(
    ofType(registerUserFailure),
    map(action => action.payload),
    tap( () => console.log(UserActionTypes.REGISTER_USER_FAILURE + ' Effect - register user failed') ),
    tap((error: Error) => this.errorDialogService.openDialog(error.message))); // TODO show message that user can understand

  @Effect()
  getUsers = this.actions$.pipe(
    ofType(getUsers),
    map(action => action.payload),
    tap( (line) => console.log(UserActionTypes.GET_USERS + ' Effect - get users of line ' + line) ),
    exhaustMap((line: string) =>
      this.userService.getUsers(line).pipe(
        map((users: UserManagement[]) => shareSuccess({payload: users})),
        catchError((error: Error) => of(shareFailure({payload: error})))
      )
    )
  );

  @Effect({ dispatch: false })
  getUsersSuccess$ = this.actions$.pipe(
    ofType(shareSuccess),
    tap( () => console.log(UserActionTypes.ACTION_SUCCESS + ' Effect - get users success') ),
    tap(() => console.log('ok') )); // TODO show message success

  @Effect({ dispatch: false })
  getUsersFailure$ = this.actions$.pipe(
    ofType(shareFailure),
    map(action => action.payload),
    tap( () => console.log(UserActionTypes.ACTION_FAILURE + ' Effect - get users failed') ),
    tap((error: Error) => this.errorDialogService.openDialog(error.message))); // TODO show message that user can understand

  @Effect()
  addRemoveGrant = this.actions$.pipe(
    ofType(addRemoveUserGrant),
    map(action => action.payload),
    tap( () => console.log(UserActionTypes.ADD_REMOVE_USER_GRANT + ' Effect add remove user grant')),
    exhaustMap( (addRemoveGrant: AddRemoveGrant) =>
      this.userService.addRemoveUserGrant(addRemoveGrant).pipe(
        map( () => shareSuccess({ payload: null} )), // TODO
        catchError((error: Error) => of(shareFailure({payload: error}))) // TODO
      )
    )
  );

  @Effect()
  userGrantsComponent = this.actions$.pipe(
    ofType(openUserGrantsComponent),
    tap( () => console.log(UserActionTypes.OPEN_USER_GRANTS_COMPONENT + ' Effect')),
    map(action => action.payload),
    mergeMap((user: UserManagement) => {
      const component = UserGrantsComponent;
      const config: MatDialogConfig = {
        width: '90%',
        height: '85%',
        data: user
      };
      return of(openDialog({payload: {component, config}}));
    })
  );
}
