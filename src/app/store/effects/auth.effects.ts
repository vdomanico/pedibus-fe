import {Actions, createEffect, Effect, ofType} from '@ngrx/effects';
import {Injectable} from '@angular/core';
import {AuthService} from '../../auth.service';
import {Router} from '@angular/router';
import {of} from 'rxjs';
import {catchError, map, exhaustMap, tap, mapTo, concatMapTo, mergeMap} from 'rxjs/operators';
import {
  logIn,
  AuthActionTypes,
  persistUser,
  deleteUser,
  logOut, mergeUser, registerChild, getUserInfo
} from '../actions/auth.actions';
import {Credentials} from '../../model/Credentials';
import {Store} from '@ngrx/store';
import {AppState} from '../app.states';
import {cleanUsersStorage} from '../actions/user.actions';
import {shareFailure, shareSuccess} from '../actions/rest.actions';
import {Child} from '../../model/Child';
import {User} from '../../model/User';
import {SocketClientService} from '../../services/socketclient/socket-client.service';
import {cleanMessagesCount, unreadMessages} from '../actions/message.actions';

@Injectable()
export class AuthEffects {

  constructor(
    private actions$: Actions,
    private socketClientService: SocketClientService,
    private authService: AuthService,
    private store: Store<AppState>,
    private router: Router,
  ) {}

  @Effect()
  login$ = this.actions$.pipe(
    ofType(logIn),
    tap( () => console.log(AuthActionTypes.LOGIN + ' Effect - login') ),
    map(action => action.payload),
    tap( x => {
      this.router.navigateByUrl('/landing');
    }),
    mergeMap(payload => [persistUser( {payload}),
      unreadMessages({ payload: +payload.unreadMessages })])
  );

  @Effect()
  logOut$ = this.actions$.pipe(
    ofType(logOut),
    tap(() => this.router.navigateByUrl('/')),
    tap( () => console.log(AuthActionTypes.LOGOUT + ' Effect - logout')),
    tap( () => this.socketClientService.disconnectClient()),
    mergeMap( () => ([deleteUser(), cleanUsersStorage(), cleanMessagesCount()])));

  @Effect()
  userInfo$ = this.actions$.pipe(
    ofType(getUserInfo),
    exhaustMap(() =>
      this.authService.getUserInfo().pipe(
        map(payload => mergeUser( {payload})),
        catchError((error) => of(shareFailure(error)))
      )));

  @Effect({ dispatch: false })
  persistedUser$ = this.actions$.pipe(
    ofType(persistUser),
    map(action => action.payload),
    tap( () => console.log(AuthActionTypes.PERSIST_USER + ' Effect - save user to local storage')),
    tap( (user) => { localStorage.setItem('user', JSON.stringify(user));
    }));

  @Effect({ dispatch: false })
  mergedUser$ = this.actions$.pipe(
    ofType(mergeUser),
    map(action => action.payload),
    tap( () => console.log(AuthActionTypes.MERGE_USER + ' Effect - marge user to local storage')),
    tap( (user) => { localStorage.setItem('user', JSON.stringify(user));
    }));

  @Effect({ dispatch: false })
  deletedUser$ = this.actions$.pipe(
    ofType(deleteUser),
    tap( () => console.log(AuthActionTypes.DELETE_USER + ' Effect - remove user from local storage')),
    tap( () => {
      localStorage.removeItem('user');
      localStorage.removeItem('currentUser');
      localStorage.removeItem('token');
      localStorage.removeItem('unreadMessages');
    }));

  @Effect()
  registerChild$ = this.actions$.pipe(
    ofType(registerChild),
    map(action => action.payload),
    tap( () => console.log(AuthActionTypes.REGISTER_CHILD + ' Effect add child')),
    exhaustMap( (uc: {user: User, child: Child}) =>
      this.authService.registerChild(uc.child).pipe(
        tap( () => {
          if (uc.user.children === null || uc.user.children === undefined) {
            uc.user.children = [];
          }
          uc.user.children.push(uc.child);
        }),
        mergeMap( () => [shareSuccess({ payload: uc.child} ), mergeUser({payload: uc.user})]), // TODO
        catchError((error: Error) => of(shareFailure({payload: error}))) // TODO
      )
    )
  );

}
