import {Actions, Effect, ofType} from '@ngrx/effects';
import {LineActionTypes} from '../actions/line.actions';
import {catchError, exhaustMap, map, tap} from 'rxjs/operators';
import {LineSchedule} from '../../model/LineSchedule';
import {shareFailure, shareSuccess} from '../actions/rest.actions';
import {of} from 'rxjs';
import {getLineChaperonSchedule, getLineSchedule, ScheduleActionTypes} from '../actions/schedule.actions';
import {LineService} from '../../services/line/line.service';
import {Injectable} from '@angular/core';
import {LineScheduleCalendar} from '../../model/LineScheduleCalendar';
import {UserLineSchedule} from '../../model/UserLineSchedule';

@Injectable()
export class ScheduleEffects {

  constructor(
    private actions$: Actions,
    private lineService: LineService
  ) {}

  @Effect()
  lineSchedule$ = this.actions$.pipe(
    ofType(getLineSchedule),
    map(action => action.payload),
    tap( payload =>
      console.log(ScheduleActionTypes.GET_LINE_SCHEDULE, 'Effect - get schedule of line ' + payload.line + ' of ' + payload.day)),
    exhaustMap((payload) =>
      this.lineService.getLineSchedule(payload.line, payload.day).pipe(
        map( (lS: LineSchedule) =>  shareSuccess({payload: lS})),
        catchError((error) => of(shareFailure(error)))
      )
    )
  );

  @Effect()
  lineUserSchedule$ = this.actions$.pipe(
    ofType(getLineChaperonSchedule),
    map(action => action.payload),
    tap( payload =>
      console.log(ScheduleActionTypes.GET_LINE_CHAPERON_SCHEDULE, 'Effect - get schedule of line ' + payload.line + ' of ' + payload.day)),
    exhaustMap((payload) =>
      this.lineService.getLineChaperonSchedule(payload.line, payload.day).pipe(
        map( (u: UserLineSchedule) =>  shareSuccess({payload: u})),
        catchError((error) => of(shareFailure(error)))
      )
    )
  );
}
