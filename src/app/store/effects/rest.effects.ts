import {Actions, Effect, ofType} from '@ngrx/effects';
import {shareFailure, shareSuccess, UserActionTypes} from '../actions/user.actions';
import {map, tap} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {ErrorDialogService} from '../../error/error-dialog.service';
import {RestActionTypes} from '../actions/rest.actions';

@Injectable()
export class RestEffects {

  constructor(
    private errorDialogService: ErrorDialogService,
    private actions$: Actions
  ) {}

  @Effect({dispatch: false})
  getUsersSuccess$ = this.actions$.pipe(
    ofType(shareSuccess),
    tap(() => console.log(RestActionTypes.ACTION_SUCCESS + ' Effect - get users success')),
    tap(() => console.log('ok'))); // TODO show message success

  @Effect({dispatch: false})
  getUsersFailure$ = this.actions$.pipe(
    ofType(shareFailure),
    map(action => action.payload),
    tap(() => console.log(RestActionTypes.ACTION_FAILURE + ' Effect - get users failed')),
    tap((error: Error) => this.errorDialogService.openDialog(error.message))); // TODO show message that user can understand

}
