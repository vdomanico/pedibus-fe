import {Actions, Effect, ofType} from '@ngrx/effects';
import {map, tap} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {unreadMessages} from '../actions/message.actions';

@Injectable()
export class MessageCountEffects {

  constructor(
    private actions$: Actions
  ) {}

  @Effect({dispatch: false})
  getUsersSuccess$ = this.actions$.pipe(
    ofType(unreadMessages),
    map( value => value.payload),
    tap((unread) => {
      localStorage.setItem('unreadMessages', '' + unread);
    }));
}
