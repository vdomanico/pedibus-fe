import {createAction, props} from '@ngrx/store';
import {RegisterUser} from '../../model/RegisterUser';
import {AddRemoveGrant} from '../../model/AddRemoveGrant';
import {UserManagement} from '../../model/UserManagement';

export enum UserActionTypes {
  REGISTER_USER = '[User] register',
  REGISTER_USER_SUCCESS = '[User] register success',
  REGISTER_USER_FAILURE = '[User] register failure',
  GET_USERS = '[Users] get line users',
  ACTION_SUCCESS = '[Users] action success',
  ACTION_FAILURE = '[User] action failure',
  CLEAN_USER_STORAGE = '[User] clean users storage',
  ADD_REMOVE_USER_GRANT = '[User] addRemove user grant',
  UPDATE_USER = '[User] update user',
  OPEN_USER_GRANTS_COMPONENT = '[User] open user grants component'
}
export const registerUser = createAction(
  UserActionTypes.REGISTER_USER,
  props<{payload: RegisterUser}>());

export const registerUserSuccess = createAction(
  UserActionTypes.REGISTER_USER_SUCCESS);

export const registerUserFailure = createAction(
  UserActionTypes.REGISTER_USER_FAILURE,
  props<{payload: Error}>());

export const getUsers = createAction(
  UserActionTypes.GET_USERS,
  props<{payload: string}>());

export const shareSuccess = createAction(
  UserActionTypes.ACTION_SUCCESS,
  props<{payload: UserManagement[]}>());

export const shareFailure = createAction(
  UserActionTypes.ACTION_FAILURE,
  props<{payload: Error}>());

export const cleanUsersStorage = createAction(
  UserActionTypes.CLEAN_USER_STORAGE
);

export const addRemoveUserGrant = createAction(
  UserActionTypes.ADD_REMOVE_USER_GRANT,
  props<{payload: AddRemoveGrant}>());

export const updateUser = createAction(
  UserActionTypes.UPDATE_USER,
  props<{payload: UserManagement}>());

export const openUserGrantsComponent = createAction(
  UserActionTypes.OPEN_USER_GRANTS_COMPONENT,
  props<{payload: UserManagement}>());
