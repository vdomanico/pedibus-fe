import {createAction, props} from '@ngrx/store';

export enum ScheduleActionTypes {
  GET_LINE_SCHEDULE = '[Line] get Line Schedule',
  GET_LINE_CHAPERON_SCHEDULE = '[Line] get Line Chaperon Schedule'
}

export const getLineSchedule = createAction(
  ScheduleActionTypes.GET_LINE_SCHEDULE,
  props<{ payload: {line: string, day: Date} }>());

export const getLineChaperonSchedule = createAction(
  ScheduleActionTypes.GET_LINE_CHAPERON_SCHEDULE,
  props<{ payload: {line: string, day: Date} }>());
