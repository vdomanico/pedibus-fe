import {createAction, props} from '@ngrx/store';

export enum MessageCountActionTypes {
  UPDATE = '[Message-Count] update',
  CLEAN = '[Message-Count] clean'
}
export const unreadMessages = createAction(
  MessageCountActionTypes.UPDATE,
  props<{ payload: number }>());

export const cleanMessagesCount = createAction(
  MessageCountActionTypes.CLEAN);
