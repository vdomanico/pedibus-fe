import {createAction, props} from '@ngrx/store';

export enum RestActionTypes {
  ACTION_SUCCESS = '[REST] action success',
  ACTION_FAILURE = '[REST] action failure',
}

export const shareSuccess = createAction(
  RestActionTypes.ACTION_SUCCESS,
  props<{ payload: any }>());

export const shareFailure = createAction(
  RestActionTypes.ACTION_FAILURE,
  props<{ payload: Error }>());

