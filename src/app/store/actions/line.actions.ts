import {createAction, props} from '@ngrx/store';
import {RegisterUser} from '../../model/RegisterUser';

export enum LineActionTypes {
  GET_LINES = '[Line] lines',
  REGISTER_LINE = '[Line] register'
}
export const getLines = createAction(
  LineActionTypes.GET_LINES);



