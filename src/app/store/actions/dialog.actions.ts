import {createAction, props} from '@ngrx/store';
import {UserActionTypes} from './user.actions';
import {Component} from '@angular/core';
import {MatDialogConfig} from '@angular/material';

export enum DialogActions {
  OPEN_DIALOG = '[Dialog] open dialog'
}

export const openDialog = createAction(
  DialogActions.OPEN_DIALOG,
  props<{payload: {component: any, config: MatDialogConfig}}>());
