import {createAction, props} from '@ngrx/store';
import {User} from '../../model/User';
import {Child} from '../../model/Child';

export enum AuthActionTypes {
  LOGIN = '[Auth] Login',
  LOGOUT = '[Auth] Logout',
  USER_INFO = '[Auth] get User Info',
  PERSIST_USER = '[Auth] Persist User (local storage)',
  MERGE_USER = '[Auth] Merge User (locale storage - preserve only token)',
  DELETE_USER = '[Auth] Delete User (local storage)',
  STORE_USER = '[Auth] Update (store)',
  REGISTER_CHILD = '[Auth] - register child',
  ADD_CHILD = '[Auth] add child to store',
  CONFIRMATION_ACCOUNT_SUCCESS = 'confirm account success',
  CONFIRMATION_ACCOUNT_KO = 'confirm account failure'
}

export const logIn = createAction(
  AuthActionTypes.LOGIN,
  props<{payload: User}>());

export const logOut = createAction(
  AuthActionTypes.LOGOUT);

export const getUserInfo = createAction(
  AuthActionTypes.USER_INFO);

export const storeUser = createAction(
  AuthActionTypes.STORE_USER,
  props<{payload: User}>());

export const persistUser = createAction(
  AuthActionTypes.PERSIST_USER,
  props<{payload: User}>());

export const mergeUser = createAction(
  AuthActionTypes.MERGE_USER,
  props<{payload: User}>());

export const deleteUser = createAction(
  AuthActionTypes.DELETE_USER);

export const registerChild = createAction(
  AuthActionTypes.REGISTER_CHILD,
  props<{payload: {user: User, child: Child}}>());

export const confirmAccountSuccess = createAction(
  AuthActionTypes.CONFIRMATION_ACCOUNT_SUCCESS);

export const confirmAccountFailure = createAction(
  AuthActionTypes.CONFIRMATION_ACCOUNT_KO,
  props<{payload: any}>());
