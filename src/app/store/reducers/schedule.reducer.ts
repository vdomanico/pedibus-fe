import {LineSchedule} from '../../model/LineSchedule';
import {ScheduleActionTypes} from '../actions/schedule.actions';
import {RestActionTypes} from '../actions/rest.actions';

export class State {
  lineSchedule: LineSchedule | null;
  errorMessage: Error | null;

  constructor(lineSchedule: LineSchedule | null, errorMessage: Error | null) {
    this.lineSchedule = lineSchedule;
    this.errorMessage = errorMessage;
  }
}

export const initialState: State = new State(
  null,
  null
);

export function scheduleReducer(state = initialState, action) {
  switch (action.type) {
    case ScheduleActionTypes.GET_LINE_SCHEDULE: {
      return new State(action.payload, null);
    }
    case RestActionTypes.ACTION_SUCCESS: {
      return new State(action.payload, null);
    }
    case RestActionTypes.ACTION_FAILURE: {
      return new State(null, action.payload);
    }
    default: {
      return state;
    }
  }
}
