import {ScheduleActionTypes} from '../actions/schedule.actions';
import {RestActionTypes} from '../actions/rest.actions';
import {UserLineSchedule} from '../../model/UserLineSchedule';

export class State {
  userLineSchedule: UserLineSchedule | null;
  errorMessage: Error | null;

  constructor(userLineSchedule: UserLineSchedule | null, errorMessage: Error | null) {
    this.userLineSchedule = userLineSchedule;
    this.errorMessage = errorMessage;
  }
}

export const initialState: State = new State(
  null,
  null
);

export function userScheduleReducer(state = initialState, action) {
  switch (action.type) {
    case ScheduleActionTypes.GET_LINE_CHAPERON_SCHEDULE: {
      return new State(action.payload, null);
    }
    case RestActionTypes.ACTION_SUCCESS: {
      return new State(action.payload, null);
    }
    case RestActionTypes.ACTION_FAILURE: {
      return new State(null, action.payload);
    }
    default: {
      return state;
    }
  }
}
