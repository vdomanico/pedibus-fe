import {UserActionTypes} from '../actions/user.actions';
import {UserManagement} from '../../model/UserManagement';

export class State {
  users: UserManagement[] | null;
  errorMessage: Error | null;
  constructor(users: UserManagement[],
              errorMessage: Error) {
    this.users = users;
    this.errorMessage = errorMessage;
  }
}

export const initialState: State = new State(
  null,
  null);

export function userReducer(state = initialState, action) {
  switch (action.type) {
    case UserActionTypes.ACTION_SUCCESS: {
      return new State(action.payload, null);
    }
    case UserActionTypes.ACTION_FAILURE: {
      return new State(null, action.payload);
    }
    case UserActionTypes.CLEAN_USER_STORAGE: {
      return new State(null, null);
    }
    case UserActionTypes.UPDATE_USER: {
      const user: UserManagement = action.payload;
      const current = state.users.find(value => value.email.match(user.email));
      const index = state.users.indexOf(current);
      state.users[index] = user;
      return new State(state.users, null);
    }
    default:
      return state;
  }
}
