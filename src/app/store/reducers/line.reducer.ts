import {LineActionTypes} from '../actions/line.actions';
import {RestActionTypes} from '../actions/rest.actions';
export class State {
  lines: string[] | null;
  errorMessage: Error | null;
  constructor(lines: string[],
              errorMessage: Error) {
    this.lines = lines;
    this.errorMessage = errorMessage;
  }
}

export const initialState: State = new State(
  null,
  null);

export function lineReducer(state = initialState, action) {
  switch (action.type) {
    case LineActionTypes.GET_LINES: {
      return new State(action.payload, null);
    }
    case RestActionTypes.ACTION_SUCCESS: {
      return new State(action.payload, null);
    }
    case RestActionTypes.ACTION_FAILURE: {
      return new State(null, action.payload);
    }
    default:
      return state;
  }
}
