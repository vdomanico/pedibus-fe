import {MessageCountActionTypes} from '../actions/message.actions';

export class State {
  unread = 0;

  constructor(unread: number) {
    this.unread = unread;
  }
}
export const initialState: State = new State(0);

export function messageCountReducer(state = initialState, action) {
  switch (action.type) {
    case MessageCountActionTypes.UPDATE: {
      return new State(action.payload);
    }
    case MessageCountActionTypes.CLEAN: {
      return new State(0);
    }
    default:
      return state;
  }
}
