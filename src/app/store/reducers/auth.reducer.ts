import {User} from '../../model/User';
import {AuthActionTypes} from '../actions/auth.actions';

export class State {
  isAuthenticated: boolean;
  user: User | null;
  errorMessage: Error | null;
  constructor(isAuthenticated: boolean,
              user: User,
              errorMessage: Error) {
    this.isAuthenticated = isAuthenticated;
    this.user = user;
    this.errorMessage = errorMessage;
  }
}

export const initialState: State = new State(
  false,
  null,
  null);

export function authReducer(state = initialState, action) {

  console.log('authReducer ' + action.type);

  switch (action.type) {

    case AuthActionTypes.LOGIN: {
      return new State(true, action.payload, null);
    }
    case AuthActionTypes.LOGOUT: {
      return new State(false, null, null);
    }
    case AuthActionTypes.STORE_USER: {
      const isAuth = action.payload !== null && action.payload !== undefined;
      return new State(isAuth, action.payload, null);
    }
    case AuthActionTypes.PERSIST_USER: {
      return new State(true, action.payload, null);
    }
    case AuthActionTypes.MERGE_USER: {
      const merged = action.payload;
      merged.token = state.user.token;
      merged.children = state.user.children;
      return new State(true, merged, null);
    }
    case AuthActionTypes.DELETE_USER: {
      return new State(false, null, null);
    }

    default:
      return state;

  }
}


