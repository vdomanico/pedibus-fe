import { Injectable } from '@angular/core';
import {MatDialog} from '@angular/material';
import {ErrorDialogComponent} from './error.dialog.component';

@Injectable({
  providedIn: 'root'
})
export class ErrorDialogService {

  constructor(public dialog: MatDialog) { }

  public openDialog(message: string): void {
    console.log('openDialog()');
    const dialogRef = this.dialog.open(ErrorDialogComponent, {
      width: '300px',
      height: '300px',
      data: message
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}
