import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';
import {map} from 'rxjs/operators';
import {User} from './model/User';
import {AppState} from './store/app.states';
import {State, Store} from '@ngrx/store';

@Injectable()
export class AccessGuard implements CanActivate {

  constructor(
    private store: Store<AppState>,
    private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    const userRoles = this.store.select(appState => appState.authState.user).pipe(
      map( (user: User) => {
        return Array.from(Object.keys(user.roles));
      }));

    for (const entry of route.data.roles) {
      if (userRoles.pipe(
        map(value => value.includes(entry)))) {
        return true;
      }
    }

    return false;
  }

}


