import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {LineReservationsComponent} from './components/line-reservations/line-reservations.component';
import {AuthGuard} from './auth.guard';
import {UserReservationComponent} from './components/user-reservation/user-reservation.component';
import {LandingComponent} from './components/landing/landing.component';
import {UserRegistrationComponent} from './components/user-registration/user-registration.component';
import {AdministrationComponent} from './components/administration/administration.component';
import {UsersControlPanelComponent} from './components/users-control-panel/users-control-panel.component';
import {ShiftsAdministrationComponent} from './components/shifts-administration/shifts-administration.component';
import {AvailabilitiesChaperonComponent} from './components/availabilities-chaperon/availabilities-chaperon.component';
import {ParentChildrenComponent} from './components/parent-children/parent-children.component';
import {ParentReservationComponent} from './components/parent-reservation/parent-reservation.component';
import {AccountConfirmationComponent} from './components/account-confirmation/account-confirmation.component';
import {ChildrenMonitorComponent} from './components/children-monitor/children-monitor.component';
import {CommunicationsComponent} from './components/communications/communications.component';
import {PersonalDataComponent} from './components/personal-data/personal-data.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent},
  { path: 'confirm/:token', component: AccountConfirmationComponent},
  { path: 'personal-data', component: PersonalDataComponent, canActivate: [AuthGuard], data: {animation: 'Landing'}},
  { path: 'register', component: UserRegistrationComponent, canActivate: [AuthGuard], data: {animation: 'Landing'}},
  { path: 'messages', component: CommunicationsComponent, canActivate: [AuthGuard], data: {animation: 'Landing'}},
  { path: 'shifts', component: ShiftsAdministrationComponent, canActivate: [AuthGuard], data: {animation: 'Landing'}},
  { path: 'availabilities', component: AvailabilitiesChaperonComponent, canActivate: [AuthGuard], data: {animation: 'Landing'}},
  { path: 'users-control-panel', component: UsersControlPanelComponent, canActivate: [AuthGuard], data: {animation: 'Landing'}},
  { path: 'children', component: ParentChildrenComponent, canActivate: [AuthGuard], data: {animation: 'Landing'}},
  { path: 'reservations', component: ParentReservationComponent, canActivate: [AuthGuard], data: {animation: 'Landing'}},
  { path: 'user', component: UserReservationComponent, canActivate: [AuthGuard], data: {animation: 'Landing'}},
  { path: 'landing', component: LandingComponent, canActivate: [AuthGuard]},
  { path: 'lines', component: LineReservationsComponent, canActivate: [AuthGuard], data: {animation: 'Landing'}},
  { path: 'monitor', component: ChildrenMonitorComponent, canActivate: [AuthGuard], data: {animation: 'Landing'}},
  { path: 'communications', component: CommunicationsComponent, canActivate: [AuthGuard], data: {animation: 'Landing'}},
  { path: '**', redirectTo: ''}
];

export const appRoutingProviders: any[] = [];
export const routing = RouterModule.forRoot(routes);
