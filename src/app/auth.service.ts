import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Router} from '@angular/router';
import {User} from './model/User';
import {Credentials} from './model/Credentials';
import {Store} from '@ngrx/store';
import {AppState} from './store/app.states';
import {UserRole} from './model/UserRole';
import {AuthActionTypes, logOut, storeUser} from './store/actions/auth.actions';
import {Child} from './model/Child';
import {BackendApi} from './backendApi';

@Injectable({ providedIn: 'root' })
export class AuthService {

  private jwtHelper = new JwtHelperService();

  constructor(
    private store: Store<AppState>,
    private http: HttpClient,
    private router: Router) {

  }

  /*
    Direct access to local storage
   */
  static getUser(): User {
    console.log('AuthService.getUser call');
    try {
      const serializedUser = localStorage.getItem('user');
      if (serializedUser === null) {
        console.log('AuthService.getUser - no user from local storage');
        return null;
      }
      const user: User = JSON.parse(serializedUser);
      console.log('AuthService.getUser - found user ');
      return user;
    } catch (err) {
      return null;
    }
  }

  static getAuthToken(): string {
    console.log('AuthService.getAuthToken call');
    const user = AuthService.getUser();
    if (user != null) {
      console.log('AuthService.getAuthToken token found: ' + user.token);
      return user.token;
    }
    return null;
  }

  public login(credentials: Credentials) {
    return this.http.post<User>(BackendApi.LOGIN, credentials);
  }

  public register(email: string, pwd: string, confirmationPwd: string) {
    return this.http.post(BackendApi.REGISTER, {email, pwd, confirmationPwd}, {observe: 'response'}).
    pipe(
      map(res => {
        if (res.status !== 200) {
          throw new Error(res.statusText);
        }
        return res;
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  checkIsLoggedIn() {
    const payload = this.updateUser();
    if (payload == null || !this.isValidToken(payload.token)) {
      console.log('authService.checkIsLoggedIn false');
      this.store.dispatch(logOut());
      return false;
    }
    console.log('authService.checkIsLoggedIn true');
    return true;
  }

  skipIfAlreadyLoggedIn() {
    const payload = this.updateUser();
    if (payload != null && this.isValidToken(payload.token)) {
      return this.router.navigateByUrl('landing');
    }
  }

  isValidToken(token: string) {
    return token != null && !this.jwtHelper.isTokenExpired(token);
  }

  updateUser(): User {
    const payload = AuthService.getUser();
    this.store.dispatch(storeUser({payload}));
    return payload;
  }


  registerChild(child: Child) {
    console.log(child);
    return this.http.post(BackendApi.REGISTER_CHILD, child);
  }


   getUserInfo() {
    return this.http.get<User>(BackendApi.USER_INFO);
  }

  updateUserPassword(currentPassword, newPassword, confirmPassword) {
    return this.http.put(BackendApi.UPDATE_USER_PASSWORD,
      {
        currentPassword: currentPassword,
        newPassword: newPassword,
        confirmPassword: confirmPassword
      });
  }

  addRegistrationToken(token: string) {
    return this.http.post(BackendApi.REGISTRATIONS_TOKEN, token);
  }

}
