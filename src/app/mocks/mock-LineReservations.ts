import { ChildrenDetail, LineStopReservation, LineReservationDetail } from '../model/line-stop-reservation';

export const LINE_1_STOP_RESERVATIONS: LineStopReservation[] = [
    { stopTime: '07:30',
      stopName: 'Massaua',
      children: [
        new ChildrenDetail('Giuseppe', false),
        new ChildrenDetail('Taddeo', false),
        new ChildrenDetail('Filippo', false)
      ]
    },
    { stopTime: '07:35',
      stopName: 'Politecnico',
      children: [
        new ChildrenDetail('Pasquale', false),
        new ChildrenDetail('Alberto', true),
        new ChildrenDetail('Pino', false)
      ]
    },
    { stopTime: '07:45',
      stopName: 'Ferraris',
      children: [
        new ChildrenDetail('Simone', true),
        new ChildrenDetail('Valerio', true),
        new ChildrenDetail('Napoleone', false)
      ]
    },
    { stopTime: '08:00',
      stopName: 'Scuola Vivaldi',
      children: []
    }
];

export const LINE_2_STOP_RESERVATIONS: LineStopReservation[] = [
    { stopTime: '13:00',
      stopName: 'Scuola Vivaldi',
      children: [
        new ChildrenDetail('Christian', false),
        new ChildrenDetail('Giacomo', false),
        new ChildrenDetail('Felice', false)
      ]
    },
    { stopTime: '13:15',
      stopName: 'Ferraris',
      children: [
        new ChildrenDetail('Alice', false)
      ]
    },
    { stopTime: '13:25',
      stopName: 'Politecnico',
      children: [
        new ChildrenDetail('Alberto', true),
        new ChildrenDetail('Valerio', true),
        new ChildrenDetail('Paolo', false),
      ]
    },
    { stopTime: '13:30',
      stopName: 'Massaua',
      children: []
    }
];

export const LINE_3_STOP_RESERVATIONS: LineStopReservation[] = [
  { stopTime: '07:30',
    stopName: 'Garibaldi',
    children: [
      new ChildrenDetail('Domenico', true),
      new ChildrenDetail('Taddeo', true),
      new ChildrenDetail('Filippo', false)
    ]
  },
  { stopTime: '07:35',
    stopName: 'Piazza Castello',
    children: [
      new ChildrenDetail('Pasquale', false),
      new ChildrenDetail('Alberto', false),
      new ChildrenDetail('Cosimo', true)
    ]
  },
  { stopTime: '07:45',
    stopName: 'Palazzo',
    children: [
      new ChildrenDetail('Simone', false),
      new ChildrenDetail('Valerio', true),
    ]
  },
  { stopTime: '08:00',
    stopName: 'Scuola Centro',
    children: []
  }
];

export const LINE_4_STOP_RESERVATIONS: LineStopReservation[] = [
  { stopTime: '13:00',
    stopName: 'Scuola Centro',
    children: [
      new ChildrenDetail('Daniele', false),
      new ChildrenDetail('Giacomo', false),
      new ChildrenDetail('Ludovico', true)
    ]
  },
  { stopTime: '13:15',
    stopName: 'Ferraris',
    children: [
      new ChildrenDetail('Alice', false)
    ]
  },
  { stopTime: '13:25',
    stopName: 'Politecnico',
    children: [
      new ChildrenDetail('Alberto', true),
      new ChildrenDetail('Valerio', true),
      new ChildrenDetail('Paolo', false),
    ]
  },
  { stopTime: '13:30',
    stopName: 'Garibaldi',
    children: []
  }
];

export const LINE_5_STOP_RESERVATIONS: LineStopReservation[] = [
  { stopTime: '07:30',
    stopName: 'Fontana',
    children: [
      new ChildrenDetail('Giuseppe', false),
      new ChildrenDetail('Taddeo', false),
      new ChildrenDetail('Filippo', false)
    ]
  },
  { stopTime: '07:35',
    stopName: 'Piazza Statuto',
    children: [
      new ChildrenDetail('Pasquale', false),
      new ChildrenDetail('Alberto', false),
      new ChildrenDetail('Pino', false)
    ]
  },
  { stopTime: '07:45',
    stopName: 'Porta Susa',
    children: [
      new ChildrenDetail('Simone', false),
      new ChildrenDetail('Valerio', false),
      new ChildrenDetail('Napoleone', false)
    ]
  },
  { stopTime: '08:00',
    stopName: 'Scuola Statuto',
    children: []
  }
];

export const LINE_6_STOP_RESERVATIONS: LineStopReservation[] = [
  { stopTime: '13:00',
    stopName: 'Scuola Statuto',
    children: [
      new ChildrenDetail('Christian', false),
      new ChildrenDetail('Giacomo', false),
      new ChildrenDetail('Felice', false)
    ]
  },
  { stopTime: '13:15',
    stopName: 'Ferraris',
    children: [
      new ChildrenDetail('Giuseppe', false)
    ]
  },
  { stopTime: '13:25',
    stopName: 'Politecnico',
    children: [
      new ChildrenDetail('Alberto', false),
      new ChildrenDetail('Francesco', false),
    ]
  },
  { stopTime: '13:30',
    stopName: 'Stati Uniti',
    children: []
  }
];

export const LINE_7_STOP_RESERVATIONS: LineStopReservation[] = [
  { stopTime: '07:30',
    stopName: 'Carlo Felice',
    children: [
      new ChildrenDetail('Luca', false),
      new ChildrenDetail('Taddeo', false),
      new ChildrenDetail('Giacomo', false)
    ]
  },
  { stopTime: '07:35',
    stopName: 'Porta Nuova',
    children: [
      new ChildrenDetail('Pasquale', false),
      new ChildrenDetail('Alberto', false),
      new ChildrenDetail('Pino', false)
    ]
  },
  { stopTime: '07:45',
    stopName: 'Porta Susa',
    children: [
      new ChildrenDetail('Simone', false),
      new ChildrenDetail('Valerio', false),
      new ChildrenDetail('Napoleone', false)
    ]
  },
  { stopTime: '08:00',
    stopName: 'Scuola Borsellino',
    children: []
  }
];

export const LINE_8_STOP_RESERVATIONS: LineStopReservation[] = [
  { stopTime: '13:00',
    stopName: 'Scuola Borsellino',
    children: [
      new ChildrenDetail('Giacomo', false),
      new ChildrenDetail('Felice', false)
    ]
  },
  { stopTime: '13:15',
    stopName: 'Ferraris',
    children: [
      new ChildrenDetail('Paolo', false)
    ]
  },
  { stopTime: '13:25',
    stopName: 'Susa',
    children: [
      new ChildrenDetail('Alberto', false),
      new ChildrenDetail('Valeria', false),
      new ChildrenDetail('Adriana', false),
    ]
  },
  { stopTime: '13:30',
    stopName: 'Statuto',
    children: []
  }
];

export const LINE_9_STOP_RESERVATIONS: LineStopReservation[] = [
  { stopTime: '07:30',
    stopName: 'Fontana',
    children: [
      new ChildrenDetail('Bartolomeo', false),
      new ChildrenDetail('Filippo', false)
    ]
  },
  { stopTime: '07:35',
    stopName: 'Palazzo Nuovo',
    children: [
      new ChildrenDetail('Davide', false),
      new ChildrenDetail('Anna', false),
      new ChildrenDetail('Luigi', false)
    ]
  },
  { stopTime: '07:45',
    stopName: 'Porta Palazzo',
    children: [
      new ChildrenDetail('Simone', false),
      new ChildrenDetail('Matteo', false),
      new ChildrenDetail('Francesca', false)
    ]
  },
  { stopTime: '08:00',
    stopName: 'Scuola Einaudi',
    children: []
  }
];

export const LINE_10_STOP_RESERVATIONS: LineStopReservation[] = [
  { stopTime: '13:00',
    stopName: 'Scuola Einaudi',
    children: [
      new ChildrenDetail('Christian', false),
      new ChildrenDetail('Cosimo', false)
    ]
  },
  { stopTime: '13:15',
    stopName: 'Porta Palazzo',
    children: [
      new ChildrenDetail('Chiara', false)
    ]
  },
  { stopTime: '13:25',
    stopName: 'Baloon',
    children: [
      new ChildrenDetail('Matteo', false),
      new ChildrenDetail('Francesco', false),
      new ChildrenDetail('Marta', false),
    ]
  },
  { stopTime: '13:30',
    stopName: 'Porta Susa',
    children: []
  }
];

export const LINE_1_RESERVATION_DETAIL: LineReservationDetail = {
  lineName : '1',
  lineDateSchedule : (d => new Date(d.setDate(d.getDate() - 2)) )(new Date).toLocaleDateString(),
  lineStopReservations : LINE_1_STOP_RESERVATIONS
};

export const LINE_2_RESERVATION_DETAIL: LineReservationDetail = {
  lineName : '2',
  lineDateSchedule : (d => new Date(d.setDate(d.getDate() - 2)) )(new Date).toLocaleDateString(),
  lineStopReservations : LINE_2_STOP_RESERVATIONS
};

export const LINE_3_RESERVATION_DETAIL: LineReservationDetail = {
  lineName : '3',
  lineDateSchedule : (d => new Date(d.setDate(d.getDate() - 1)) )(new Date).toLocaleDateString(),
  lineStopReservations : LINE_3_STOP_RESERVATIONS
};

export const LINE_4_RESERVATION_DETAIL: LineReservationDetail = {
  lineName : '4',
  lineDateSchedule : (d => new Date(d.setDate(d.getDate() - 1)) )(new Date).toLocaleDateString(),
  lineStopReservations : LINE_4_STOP_RESERVATIONS
};

export const LINE_5_RESERVATION_DETAIL: LineReservationDetail = {
  lineName : '5',
  lineDateSchedule : (d => new Date(d.setDate(d.getDate())))(new Date).toLocaleDateString(),
  lineStopReservations : LINE_5_STOP_RESERVATIONS
};

export const LINE_6_RESERVATION_DETAIL: LineReservationDetail = {
  lineName : '6',
  lineDateSchedule : (d => new Date(d.setDate(d.getDate())))(new Date).toLocaleDateString(),
  lineStopReservations : LINE_6_STOP_RESERVATIONS
};

export const LINE_7_RESERVATION_DETAIL: LineReservationDetail = {
  lineName : '7',
  lineDateSchedule : (d => new Date(d.setDate(d.getDate() + 1)) )(new Date).toLocaleDateString(),
  lineStopReservations : LINE_7_STOP_RESERVATIONS
};

export const LINE_8_RESERVATION_DETAIL: LineReservationDetail = {
  lineName : '8',
  lineDateSchedule : (d => new Date(d.setDate(d.getDate() + 1)) )(new Date).toLocaleDateString(),
  lineStopReservations : LINE_8_STOP_RESERVATIONS
};

export const LINE_9_RESERVATION_DETAIL: LineReservationDetail = {
  lineName : '9',
  lineDateSchedule : (d => new Date(d.setDate(d.getDate() + 2)) )(new Date).toLocaleDateString(),
  lineStopReservations : LINE_9_STOP_RESERVATIONS
};

export const LINE_10_RESERVATION_DETAIL: LineReservationDetail = {
  lineName : '10',
  lineDateSchedule : (d => new Date(d.setDate(d.getDate() + 2)) )(new Date).toLocaleDateString(),
  lineStopReservations : LINE_10_STOP_RESERVATIONS
};
