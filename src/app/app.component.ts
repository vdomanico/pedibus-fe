import {Component, HostListener, OnInit} from '@angular/core';
import {MatIconRegistry, MatSnackBar} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import {NotificationService} from './services/notification/notification.service';
import {Observable} from 'rxjs';
import {ThemeService} from './services/theme/theme.service';
import {NavigationStart, Router, RouterOutlet} from '@angular/router';
import {slider} from './animation';
import {Notification, NotificationType} from './model/Notification';
import {AppState} from './store/app.states';
import {Store} from '@ngrx/store';
import {getUserInfo} from './store/actions/auth.actions';
import {unreadMessages} from './store/actions/message.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ThemeService],
  animations: [
    slider
    // animation triggers go here
  ]
})

export class AppComponent implements OnInit {
  title = 'Pedibus';
  isDarkTheme$: Observable<boolean>;
  constructor(private iconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer,
              private notificationService: NotificationService,
              private snackBar: MatSnackBar,
              private themeService: ThemeService,
              private store: Store<AppState>,
              private router: Router) {
    this.iconRegistry.addSvgIcon('adjust',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/img/adjust.svg'));
    this.isDarkTheme$ = this.themeService.isDarkTheme;
  }

  notifications: Notification[] = [];

  ngOnInit(): void {
    this.subscribeToNotifications();
  }

  subscribeToPageRefresh() {
    this.router.events.subscribe((event) => {
      console.log('ready to dispatch');
      this.store.dispatch(unreadMessages({payload: +localStorage.getItem('unreadMessages')}));
    });
  }

  @HostListener('window:load') refreshUnreadMessages() {
    console.log('ready to refreshLocalStorage');
    this.store.dispatch(unreadMessages({payload: +localStorage.getItem('unreadMessages')}));
  }

  subscribeToNotifications() {
    this.notificationService.getNotifications()
      .subscribe(
        value => {
          value.timeout = 30000;
          switch (value.type.toString()) {
            case NotificationType.REFRESH.toString(): {
              this.store.dispatch(getUserInfo());
              this._addNotification(value);
              break;
            }
            case NotificationType.UNREAD_MESSAGES.toString(): {
              this.store.dispatch(unreadMessages({payload: +value.message}));
              break;
            }
            default : {
              this._addNotification(value);
              break;
            }
          }
        }
      );
  }

  close(notification: Notification) {
    this.notifications = this.notifications.filter(notif => notif.id !== notification.id);
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }

  private _addNotification(notification: Notification) {
    this.notifications.push(notification);

    if (notification.timeout !== 0) {
      setTimeout(() => this.close(notification), notification.timeout);

    }
  }

  className(notification: Notification): string {

    let style: string;

    switch (notification.type) {

      case NotificationType.SUCCESS:
        style = 'success';
        break;

      case NotificationType.WARNING:
        style = 'warning';
        break;

      case NotificationType.ERROR:
        style = 'error';
        break;

      default:
        style = 'info';
        break;
    }

    return style;
  }

/*  @HostListener('window:beforeunload', ['$event'])
  logout($event) {
  }*/

}
