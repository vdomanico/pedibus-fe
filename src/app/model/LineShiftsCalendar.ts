import {ShiftSchedule} from './ShiftSchedule';

export class LineShiftsCalendar {
  line: string;
  shifts: ShiftSchedule[];
}
