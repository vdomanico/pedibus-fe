export class ChildrenDetail {
  name: string;
  isPresent: boolean;

  constructor(name: string, isPresent: boolean) {
      this.name = name;
      this.isPresent = isPresent;
  }
}

export class User {
  id: number;
  email: string;
  password: string;
}

export interface LineStopReservation {
  stopName: string;
  stopTime: string;
  children: ChildrenDetail[];
}

export interface LineReservationDetail {
  lineName: string;
  lineDateSchedule: string;
  lineStopReservations: LineStopReservation[];
}
