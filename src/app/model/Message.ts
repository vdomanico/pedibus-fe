export class Message {
  id: string;
  subject: string;
  message: string;
  sentDateTime: Date;
  readDateTime: Date;
  read: boolean;

  public toString(): string {
    return JSON.stringify(this);
  }

}
