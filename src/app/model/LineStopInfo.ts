import {Time} from '@angular/common';

export class LineStopInfo {
  name: string;
  time: Time;
}
