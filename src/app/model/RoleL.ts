import {UserRole} from './UserRole';

export class RoleL {
  role: UserRole;
  lines: string[];

  constructor(role: UserRole, lines: string[]) {
    this.role = role;
    this.lines = lines;
  }

  toString() {
    return JSON.stringify(this);
  }
}
