import {UserRole} from './UserRole';
import {Child} from './Child';


export class User {
  email: string;
  name: string;
  surname: string;
  password: string;
  token: string;
  roles: { role: UserRole, lines: string[] };
  children: Child[];
  unreadMessages: number;

  public toString(): string {
    return JSON.stringify(this);
  }
}
