import {Child} from './Child';

export class UpdateReservationChildren {
  child: Child;
  place: string;
  direction: string;
  ticket: string;


  constructor(child: Child, place: string, direction: string, ticket: string) {
    this.child = child;
    this.place = place;
    this.direction = direction;
    this.ticket = ticket;
  }

  toString(): string {
    return JSON.stringify(this);
  }
}
