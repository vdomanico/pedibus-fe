export class ChildReservation {
  name: string;
  surname: string;
  email: string;
  ticket: string;
  collected: boolean;
  delivered: boolean;

  constructor(name: string,
              surname: string,
              email: string,
              ticket: string,
              collected: boolean,
              delivered: boolean) {
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.ticket = ticket;
    this.delivered = delivered;
    this.collected = delivered;
  }

  toString() {
    return JSON.stringify(this);
  }

}
