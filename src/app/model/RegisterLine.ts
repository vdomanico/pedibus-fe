import {User} from './User';

export class RegisterLine {
  line: string;
  admin: User;

  public toString(): string {
    return JSON.stringify(this);
  }
}
