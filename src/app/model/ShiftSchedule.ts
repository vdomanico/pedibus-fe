export class ShiftSchedule {
  line: string;
  date: Date;
  toHome: boolean;
  toSchool: boolean;
}
