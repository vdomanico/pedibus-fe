import {UserRole} from './UserRole';
import {RoleL} from './RoleL';

export class UserManagement {
  email: string;
  name: string;
  surname: string;
  enable: boolean;
  roles: { role: UserRole, lines: string[] };

  public toString(): string {
    return JSON.stringify(this);
  }

}
