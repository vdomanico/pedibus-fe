import {LineStopDetail} from './LineStopDetail';
import {ChildReservationStatus} from './ChildReservationStatus';

export class ReservationLineDaySchedule {
  line: string;
  day: string;
  toSchool: LineStopDetail[];
  toHome: LineStopDetail[];
  children: ChildReservationStatus[];
}
