import {LineStopInfo} from './LineStopInfo';

export class LineStops {
  line: string;
  toSchool: LineStopInfo[];
  toHome: LineStopInfo[];
}
