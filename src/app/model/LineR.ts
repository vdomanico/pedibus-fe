import {UserRole} from './UserRole';

export class LineR {
  line: string;
  roles: UserRole[];

  toString() {
    return JSON.stringify(this);
  }
}
