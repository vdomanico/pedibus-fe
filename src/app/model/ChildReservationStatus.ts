import {LineStopDetail} from './LineStopDetail';

export class ChildReservationStatus {

  date: Date;
  name: string;
  surname: string;
  email: string;
  reservedToHome: boolean;
  reservedToSchool: boolean;
  lineToHome: string;
  lineToSchool: string;
  ticketToHome: string;
  ticketToSchool: string;

}
