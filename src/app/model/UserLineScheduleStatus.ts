export class UserLineScheduleStatus {
  shift: boolean;
  available: boolean;
  reservedElseWhere: boolean;
  lineElseWhere: string;
}
