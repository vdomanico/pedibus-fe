import {UserLineScheduleStatus} from './UserLineScheduleStatus';
import {User} from './User';

export class UserLineSchedule {
  userDetail: User;
  to_school: UserLineScheduleStatus;
  to_home: UserLineScheduleStatus;
}
