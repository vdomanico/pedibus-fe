export class LineResource {
  lines: string[];

  toString(): string {
    return JSON.stringify(this);
  }
}
