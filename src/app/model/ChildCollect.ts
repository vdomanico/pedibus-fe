export class ChildCollect {
  ticket: string;
  collect: boolean;

  constructor(ticket: string, collect: boolean) {
    this.ticket = ticket;
    this.collect = collect;
  }

  toString() {
    return JSON.stringify(this);
  }

}
