import {UserRole} from './UserRole';

export class AddRemoveGrant {
  line: string;
  enable: boolean;
  email: string;
  role: UserRole;

  constructor(line: string, enable: boolean, email: string, role: UserRole) {
    this.line = line;
    this.enable = enable;
    this.email = email;
    this.role = role;
  }

  toString(): string {
    return JSON.stringify(this);
}

}
