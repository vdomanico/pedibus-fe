export class Page<T> {
  content: T[];
  size: number;
  totalPages: number;
  totalElements: number;
  empty: boolean;
  first: boolean;
  last: boolean;
  sort: {unsorted: boolean; sorted: boolean; empty: boolean};
}
