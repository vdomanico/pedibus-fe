import {UserLineSchedule} from './UserLineSchedule';

export class LineSchedule {
  line: string;
  day: string;
  users: UserLineSchedule[];
}
