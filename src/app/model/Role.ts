import {UserRole} from './UserRole';

export interface Role {
  line: string;
  email: string;
  userRole: UserRole;
}
