import {Child} from './Child';
import {Time} from '@angular/common';

export class AddReservationChildren {
  children: Child[];
  place: string;
  direction: string;


  constructor(children: Child[], place: string, direction: string) {
    this.children = children;
    this.place = place;
    this.direction = direction;
  }

  toString(): string {
    return JSON.stringify(this);
}

}
