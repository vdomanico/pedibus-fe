import {Child} from './Child';

export class ChildLineScheduleStatus {
  child: Child;
  reserved: boolean;
  ticket: string;

  constructor(child: Child, reserved: boolean, ticket: string) {
    this.child = child;
    this.reserved = reserved;
    this.ticket = ticket;
  }

}
