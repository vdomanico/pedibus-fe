export class AddRemoveAvailability {
  line: string;
  day: string;
  email: string;
  direction: string;
  add: boolean;

  constructor(line: string, day: string, email: string, direction: string, add: boolean) {
    this.line = line;
    this.day = day;
    this.email = email;
    this.direction = direction;
    this.add = add;
  }

  toString(): string {
    return JSON.stringify(this);
}

}
