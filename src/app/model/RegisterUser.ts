export class RegisterUser {
  email: string;
  pwd: string;
  name: string;
  surname: string;
  confirmationPwd: string;
  roles: Array<string>;
  line: string;

  public toString(): string {
     return JSON.stringify(this);
  }

}
