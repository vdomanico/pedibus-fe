export class Notification {

  constructor(
    public id: number,
    public type: NotificationType,
    public title: string,
    public message: string,
    public timeout: number,
  ) { }

}

export enum NotificationType {
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
  WARNING = 'WARNING',
  INFO = 'INFO',
  REFRESH = 'REFRESH',
  MONITOR = 'MONITOR',
  UNREAD_MESSAGES = 'UNREAD_MESSAGES'
}
