import {Child} from './Child';
import {ChildReservation} from './ChildReservation';

export class LineStopDetail {
  place: string;
  time: string;
  onBoard: ChildReservation[];
  offBoard: ChildReservation[];
  latitude: number;
  longitude: number;
}
