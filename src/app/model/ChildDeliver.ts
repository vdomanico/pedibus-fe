export class ChildDeliver {
  ticket: string;
  deliver: boolean;

  constructor(ticket: string, deliver: boolean) {
    this.ticket = ticket;
    this.deliver = deliver;
  }

  toString() {
    return JSON.stringify(this);
  }

}
