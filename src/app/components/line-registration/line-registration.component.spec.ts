import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineRegistrationComponent } from './line-registration.component';

describe('LineRegistrationComponent', () => {
  let component: LineRegistrationComponent;
  let fixture: ComponentFixture<LineRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
