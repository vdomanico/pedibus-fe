import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/app.states';
import {MatSnackBar} from '@angular/material';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {LineActionTypes} from '../../store/actions/line.actions';
import {RegisterLine} from '../../model/RegisterLine';
import {UserManagement} from '../../model/UserManagement';

@Component({
  selector: 'app-line-registration',
  templateUrl: './line-registration.component.html',
  styleUrls: ['./line-registration.component.css']
})
export class LineRegistrationComponent implements OnInit {

  constructor(private _formBuilder: FormBuilder,
              private router: Router,
              private store: Store<AppState>,
              private snackBar: MatSnackBar) { }

  admins: UserManagement[];
  admin: UserManagement;
  lineFormGroup: FormGroup;
  lines$: Observable<string[]>;
  private line: RegisterLine = new RegisterLine();


  ngOnInit() {
  this.lines$ = this.getLineOptions();
    this.lineFormGroup = this._formBuilder.group({
      lineCtrl: ['', [Validators.required, Validators.minLength(1)]],
      adminCtrl: ['', Validators.required]
    }, );
    this.getAdmins();
  }

  onSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  getLineOptions(): Observable<string[]> {
    return this.store.select(state => state.lineState.lines).pipe(
      map(rMap => Object.keys(rMap)
        .map(value => rMap[value])));
  }

  onRegisterLine() {
    if (!this.lineFormGroup.invalid) {
      this.line.line = this.lineFormGroup.value.lineCtrl;
      this.line.admin = this.lineFormGroup.value.adminCtrl;
      this.store.dispatch({type: LineActionTypes.REGISTER_LINE, payload: this.line});
      this.snackBar.open('Registrazione linea effettuata!', 'Torna alla home', {
        duration: 4000,
      }).afterDismissed().subscribe(() => {
        console.log('snackBar dismissed');
        this.router.navigate(['/landing']);
      });
    }
  }

  getAdmins() {
    this.store.subscribe(state => {
      this.admins = state.userState.users != null ? state.userState.users : [];
    });
  }


  selectedAdmin(admin: UserManagement) {
    console.log('Admin selezionato: ' + admin.name + ' ' + admin.surname + ' ' + admin.email);
    this.admin = admin;
  }
}
