import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-map-dialog',
  templateUrl: './map-dialog.component.html',
  styleUrls: ['./map-dialog.component.css']
})
export class MapDialogComponent implements OnInit {

  lat: number;
  lon: number;

  constructor(public dialogRef: MatDialogRef<MapDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data) {
    this.lat = data.latitude;
    this.lon = data.longitude;
    console.log('lat lon: ' + this.lat, this.lon);
  }

  ngOnInit() {
  }

}
