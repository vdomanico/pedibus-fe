import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ChildReservation} from '../../model/ChildReservation';
import {ReservationLineDaySchedule} from '../../model/ReservationLineDaySchedule';
import {LineStopDetail} from '../../model/LineStopDetail';


@Component({
  selector: 'app-edit-reservation',
  templateUrl: './edit-reservation.component.html',
  styleUrls: ['./edit-reservation.component.css']
})
export class EditReservationComponent implements OnInit {

  stops: LineStopDetail[];
  child: ChildReservation;
  reservation: ReservationLineDaySchedule;
  toSchool: boolean;
  localData: any;
  selectedPlace = '';

  constructor(public dialogRef: MatDialogRef<EditReservationComponent>,
    @Inject(MAT_DIALOG_DATA) private data) {
    this.dialogRef.backdropClick().subscribe(() => {
      this.dialogRef.close('CANCEL');
    });
  }

  ngOnInit() {
    this.localData = this.data;
    this.child = this.localData.child;
    this.reservation = this.localData.reservation;
    this.toSchool = this.localData.toSchool;
    if (this.toSchool) {
      this.stops = this.localData.reservation.toSchool;
    } else {
      this.stops = this.localData.reservation.toHome;
    }
  }

  isToSchool(): boolean {
    return this.toSchool;
  }

  isToHome(): boolean {
    return !this.toSchool;
  }

  updateStop(place: string) {
    this.selectedPlace = place;
  }

  closeAndConfirm() {
    console.log('THIS.DATA.PLACE: ' + this.selectedPlace);
    this.dialogRef.close(this.selectedPlace);
  }

  closeAndDelete() {
    this.dialogRef.close('DELETE');
  }

  closeAndCancel() {
    this.dialogRef.close('CANCEL');
  }
}
