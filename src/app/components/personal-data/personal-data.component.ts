import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../auth.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['./personal-data.component.css']
})
export class PersonalDataComponent implements OnInit {

  userFormGroup: FormGroup;
  hideO = true;
  hideP = true;
  hideC = true;

  constructor(private _formBuilder: FormBuilder,
              private authService: AuthService,
              private snackBar: MatSnackBar) { }

  static validateIfEqualPassword(group: FormGroup) {
    if (group.value.newPasswordCtrl === group.value.confirmationPasswordCtrl) {
      return null;
    }
    return  {'equalPassword': true};
  }


  ngOnInit() {
    this.userFormGroup = this._formBuilder.group({
      currentPasswordCtrl: ['', [Validators.required, Validators.minLength(5)]],
      newPasswordCtrl: ['', [Validators.required, Validators.minLength(5)]],
      confirmationPasswordCtrl: ['', [Validators.required, Validators.minLength(5)]],
    }, {validator: [PersonalDataComponent.validateIfEqualPassword]});
  }

  onUpdatePassword() {
    if (!this.userFormGroup.invalid) {
      this.authService.updateUserPassword(
        this.userFormGroup.value.currentPasswordCtrl,
        this.userFormGroup.value.newPasswordCtrl,
        this.userFormGroup.value.confirmationPasswordCtrl,
      ).subscribe(
          data => {
            this.snackBar.open('Aggiornamento Effettuato!', 'Ok', {
              duration: 4000,
            });
          },
        error => {
          this.snackBar.open('Password Attuale Sbagliata', 'Ok', {
            duration: 4000,
          });
        }
      );
    }
  }

}
