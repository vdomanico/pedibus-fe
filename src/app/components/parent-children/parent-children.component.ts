import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import {FormGroup, Validators, FormControl, FormBuilder} from '@angular/forms';
import {AppState} from '../../store/app.states';
import {Child} from '../../model/Child';
import {registerChild} from '../../store/actions/auth.actions';
import {User} from '../../model/User';


@Component({
  selector: 'app-parent-children',
  templateUrl: './parent-children.component.html',
  styleUrls: ['./parent-children.component.css']
})
export class ParentChildrenComponent implements OnInit {

  childGroup: FormGroup;
  user: User;
  children: Array<Child>;

  constructor(private formBuilder: FormBuilder,
    private store: Store<AppState>) { }

  ngOnInit() {
    this.store.subscribe(appState => {
      this.user = appState.authState.user;
      this.children = appState.authState.user.children;
    });
    this.childGroup = this.formBuilder.group({
      nameC: ['', [Validators.required, Validators.minLength(1)]],
        surnameC: ['', [Validators.required, Validators.minLength(1)]],
    }, {validator: []});
  }

  onRegisterChild() {
    if (!this.childGroup.invalid) {
      const child = new Child(
        this.childGroup.value.nameC,
        this.childGroup.value.surnameC,
        this.user.email);
      const user = this.user;
      this.store.dispatch(registerChild({payload: {user, child}}));
      this.childGroup.reset();
    }
  }

  childNotConflict() {
    this.normalizeForm(this.childGroup);
    return this.children === null ||
      this.children === undefined ||
      this.children.length === 0 ||
      !Object.values(this.children).find( (value: Child) => {
          return value.name === this.childGroup.value.nameC && value.surname === this.childGroup.value.surnameC;
        }
      );
  }

  public normalizeForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((key) => {
      const k = formGroup.get(key);
      k.setValue(formGroup.get(key).value.trim());
      k.setValue(formGroup.get(key).value.toUpperCase());
    });
  }

}
