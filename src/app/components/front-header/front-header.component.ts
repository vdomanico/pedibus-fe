import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../auth.service';
import {ErrorDialogService} from '../../error/error-dialog.service';
import {Router} from '@angular/router';
import {AuthActionTypes} from '../../store/actions/auth.actions';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/app.states';
import {Observable} from 'rxjs';
import {Subscription} from 'rxjs';
import {fromEvent} from 'rxjs';
import {User} from '../../model/User';
import {ThemeService} from '../../services/theme/theme.service';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-front-header',
  templateUrl: './front-header.component.html',
  styleUrls: ['./front-header.component.css']
})


export class FrontHeaderComponent implements OnInit, OnDestroy {

  private user$: Observable<User>;
  @Input() private back: string;
  isDarkTheme: Observable<boolean>;
  isMobile: boolean;
  private unreadMessages$: Observable<number>;

  constructor(private authService: AuthService,
              private errorDialogService: ErrorDialogService,
              private router: Router,
              private store: Store<AppState>,
              private themeService: ThemeService) {
  }

  resizeObservable$: Observable<Event>;
  resizeSubscription$: Subscription;

  ngOnInit() {
    this.isDarkTheme = this.themeService.isDarkTheme;
    this.user$ = this.store.select(state => state.authState.user);

    this.resizeObservable$ = fromEvent(window, 'resize');
    this.resizeSubscription$ = this.resizeObservable$.subscribe( evt => {
      if (window.innerWidth < 600) {
        this.isMobile = true;
      } else {
        this.isMobile = false;
      }
    });

    if (window.innerWidth < 600) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }

    this.unreadMessages$ = this.store.select(appState => appState.messageCountState.unread);
}

  ngOnDestroy() {
    this.resizeSubscription$.unsubscribe();
  }


  public onLogout() {
    console.log('onLogout()');
    this.store.dispatch({type: AuthActionTypes.LOGOUT});
  }

  goBack() {
    this.router.navigateByUrl('/landing'); // FIXME use effects
  }

  goToCommunications() {
    this.router.navigateByUrl('/communications');
  }

  toggleDarkTheme(checked: boolean) {
    this.themeService.setDarkTheme(checked);
    console.log('Switching...' + checked);
  }

}
