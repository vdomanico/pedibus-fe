import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailabilitiesChaperonComponent } from './availabilities-chaperon.component';

describe('AvailabilitiesChaperonComponent', () => {
  let component: AvailabilitiesChaperonComponent;
  let fixture: ComponentFixture<AvailabilitiesChaperonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailabilitiesChaperonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailabilitiesChaperonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
