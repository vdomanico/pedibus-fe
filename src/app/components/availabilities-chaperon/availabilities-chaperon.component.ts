import {AfterContentInit, Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import {UserRole} from '../../model/UserRole';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/app.states';
import {LineService} from '../../services/line/line.service';
import {DateAdapter, MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatTableDataSource} from '@angular/material';
import {UserLineSchedule} from '../../model/UserLineSchedule';
import {catchError, map} from 'rxjs/operators';
import {getLineChaperonSchedule, getLineSchedule} from '../../store/actions/schedule.actions';
import {BehaviorSubject, throwError} from 'rxjs';
import {FormControl} from '@angular/forms';
import {formatDate} from '@angular/common';
import {AddRemoveAvailability} from '../../model/AddRemoveAvailability';
import {Router} from '@angular/router';
import {ErrorDialogComponent} from '../../error/error.dialog.component';

@Component({
  selector: 'app-availabilities-chaperon',
  templateUrl: './availabilities-chaperon.component.html',
  styleUrls: ['./availabilities-chaperon.component.css']
})
export class AvailabilitiesChaperonComponent implements OnInit, AfterContentInit {

  line: string;
  lines: string[];
  day: Date;
  filter;
  days$: BehaviorSubject<Date[]> = new BehaviorSubject<Date[]>([]);
  dateFormControl = new FormControl(new Date().toISOString());
  chaperonLineSchedule: UserLineSchedule;

  constructor(
    @Inject(LOCALE_ID) private locale: string,
    private store: Store<AppState>,
    private lineService: LineService,
    private _adapter: DateAdapter<any>,
    public errorDialog: MatDialog,
    private router: Router) {
    // this.dataSource = new MatTableDataSource<UserLineSchedule>();
    this._adapter.setLocale('it');
  }

  ngOnInit() {
    console.log('AvailabilitiesChaperonComponent.ngOnInit()');
    this.store.subscribe(
      s => {
        const roles = s.authState.user.roles;
        this.lines =  [].concat.apply([], Object.keys(roles).filter(value => value.match(UserRole.CHAPERON))
          .map(value => roles[value]));
        this.chaperonLineSchedule = s.userScheduleState.userLineSchedule;
      }
    );
    this.days$.subscribe(
      value => {
        this.filter = null;
        this.filter = (d: Date): boolean => {
          const formattedDay = formatDate(d, 'yyyy-MM-dd', this.locale);
          const allowedDays = value.map(c => {
            return formatDate(c, 'yyyy-MM-dd', this.locale);
          });
          const today = new Date().setHours(0, 0, 0, 0);
          return (d.getTime() >= today && allowedDays.includes(formattedDay));
        };
      });
  }

  ngAfterContentInit(): void {
    console.log('AvailabilitiesChaperonComponent.ngAfterContentInit()');
    this.line = this.lines.length > 0 ? this.line = this.lines[0] : null;
    this.updateLineCalendar();
  }

  updateLineCalendar() {
    console.log('AvailabilitiesChaperonComponent.updateLineCalendar() line: ' + this.line.toString());
    if (this.line !== null) {
      this.lineService.getLineScheduleCalendar(this.line).pipe(
        map(payload => payload.days)).
      subscribe(
        days => {
          this.days$.next(days);
          if (days.length > 0) {
            this.day = days[0];
            this.changeDayLineSchedule();
          }
        }
      );
    }
  }

  changeDayLineSchedule() {
    const line = this.line;
    const day = this.dateFormControl.value;
    this.day = day;
    if (line !== null && day !== null) {
      console.log('AvailabilitiesChaperonComponent - ready to dispatch, line:' + this.line + ' day' + this.day);
      this.store.dispatch(getLineChaperonSchedule({ payload: {line, day}}));
    }
  }

  /* called by template*/
  changeLineSchedule(line: string) {
    console.log('ShiftsAdministrationComponent.changeLineSchedule() line: ' + line);
    this.line = line;
    this.updateLineCalendar();
  }

  /* called by template */
  changeDaySchedule() {
    console.log('ShiftsAdministrationComponent.changeDaySchedule() day: ' + this.day.toString());
    this.changeDayLineSchedule();
  }

  addRemoveAvailability(to_school: boolean) {
    const add: boolean = to_school ?
      !this.chaperonLineSchedule.to_school.available :
      !this.chaperonLineSchedule.to_home.available;
    const direction = to_school ? 'TO_SCHOOL' : 'TO_HOME';


    this.lineService.addRemoveAvailability(
      new AddRemoveAvailability(
        this.line.toString(),
        formatDate(this.day, 'yyyy-MM-dd', this.locale),
        this.chaperonLineSchedule.userDetail.email,
        direction,
        add)
    ).pipe(
      catchError(err => {
        console.log(`addRemoveAvailability ${err}`);
        const d = this.errorDialog.open(ErrorDialogComponent, {
          width: '250px',
          height: '200px',
          data: 'Impossibile modificare la disponibilità - il turno è stato già consolidato'
        });
          return throwError(err);
      })
    ).subscribe(
      value => {
        if (to_school) {
          console.log('toSchool ' + add);
          this.chaperonLineSchedule.to_school.available = add;
        } else {
          this.chaperonLineSchedule.to_home.available = add;
          console.log('toHome ' + add);
        }
      }
    );
  }
}
