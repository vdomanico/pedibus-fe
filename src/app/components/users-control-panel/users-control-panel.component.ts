import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/app.states';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {UserManagement} from '../../model/UserManagement';
import {getUsers} from '../../store/actions/user.actions';
import {getLines} from '../../store/actions/line.actions';
import {UserGrantsComponent} from '../user-grants/user-grants.component';

@Component({
  selector: 'app-users-control-panel',
  templateUrl: './users-control-panel.component.html',
  styleUrls: ['./users-control-panel.component.css']
})
export class UsersControlPanelComponent implements OnInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('dialogButton', {static: true}) dialogButton: ElementRef;

  dataSource;
  line: string;
  lines: string[];
  columnsToDisplay = ['line', 'email', 'user', 'enabled', 'role'];
  loggedUser;
  $store;
  constructor(private store: Store<AppState>,
              public dialog: MatDialog) {
    this.dataSource = new MatTableDataSource<UserManagement>();
  }

  ngOnInit() {
    this.$store = this.store.subscribe(state => {
      console.log('UsersControlPanelComponent.subscribe users ngOnInit');
      this.lines = state.lineState.lines;
      this.lines != null && this.lines.length > 0 ? this.line = this.lines[0] : this.line = null;
      this.dataSource.data = state.userState.users != null ? state.userState.users : [];
      this.dataSource.paginator = this.paginator;
      this.dataSource.sortingDataAccessor = (item, property) => {
        if (property === 'user') {
          return item.surname;
        } else {
          return item[property];
        }
      };
      this.dataSource.sort = this.sort;
      this.loggedUser = state.authState.user;
      }
    );
    this.store.dispatch(getLines());
    this.store.dispatch( getUsers({payload: this.line}));
  }

  onChangeLine(line: string) {
    this.line = line;
    this.store.dispatch( getUsers({payload: line}));
  }

  openAddRemoveGrantsPanel(user: UserManagement) {
    // this.store.dispatch(openUserGrantsComponent({payload: user}));

    const d = this.dialog.open(UserGrantsComponent,  {
      width: '500px',
      height: '500px',
      data: user
    });
  }
}
