import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/user/user.service';
import {catchError, tap} from 'rxjs/operators';
import {AppState} from '../../store/app.states';
import {Store} from '@ngrx/store';
import {confirmAccountFailure, confirmAccountSuccess} from '../../store/actions/auth.actions';
import {throwError} from 'rxjs';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-account-confirmation',
  templateUrl: './account-confirmation.component.html',
  styleUrls: ['./account-confirmation.component.css']
})
export class AccountConfirmationComponent {

  constructor(private route: ActivatedRoute,
              private userService: UserService,
              private store: Store<AppState>) {
    console.log('AccountConfirmationComponent.constructor');
    this.route.params.subscribe(params => {
      const token = params['token'];
      this.userService.confirmAccount(token).pipe(
        tap(x => this.store.dispatch( confirmAccountSuccess())),
        catchError((error: HttpErrorResponse) => {
          this.store.dispatch(confirmAccountFailure({payload: error}));
          return throwError(error);
        })
      ).subscribe();
    });
  }

}
