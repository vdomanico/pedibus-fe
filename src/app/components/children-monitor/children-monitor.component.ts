import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-children-monitor',
  templateUrl: './children-monitor.component.html',
  styleUrls: ['./children-monitor.component.css']
})
export class ChildrenMonitorComponent implements OnInit {

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }


  clickMe() {
    this.http.get('wstest').subscribe();
  }

}
