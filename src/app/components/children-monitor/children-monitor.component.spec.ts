import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildrenMonitorComponent } from './children-monitor.component';

describe('ChildrenMonitorComponent', () => {
  let component: ChildrenMonitorComponent;
  let fixture: ComponentFixture<ChildrenMonitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildrenMonitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildrenMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
