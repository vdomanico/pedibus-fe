import {AfterContentInit, Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import {UserRole} from '../../model/UserRole';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/app.states';
import {catchError, filter, map, tap} from 'rxjs/operators';
import {LineService} from '../../services/line/line.service';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {FormControl} from '@angular/forms';
import {Child} from '../../model/Child';
import {DatePipe, formatDate} from '@angular/common';
import {ReservationLineDaySchedule} from '../../model/ReservationLineDaySchedule';
import {AddReservationChildren} from '../../model/AddReservationChildren';
import {LineStopInfo} from '../../model/LineStopInfo';
import {LineStops} from '../../model/LineStops';
import {ChildLineScheduleStatus} from '../../model/ChildLineScheduleStatus';
import * as _ from 'lodash';
import {ChildReservation} from '../../model/ChildReservation';
import {DateAdapter, MatDialog} from '@angular/material';
import {ErrorDialogComponent} from '../../error/error.dialog.component';
import {MapDialogComponent} from '../map-dialog/map-dialog.component';
import {LineStopDetail} from '../../model/LineStopDetail';

@Component({
  selector: 'app-parent-reservation',
  templateUrl: './parent-reservation.component.html',
  styleUrls: ['./parent-reservation.component.css'],
  providers: [DatePipe]
})
export class ParentReservationComponent implements OnInit, AfterContentInit {

  line: string;
  lines: string[];
  childrenWithStatus: ChildLineScheduleStatus[] = null;
  childrenWithStatusFiltered: ChildLineScheduleStatus[] = [];
  day: Date;
  days$: BehaviorSubject<Date[]> = new BehaviorSubject<Date[]>([]);
  filter;
  dateFormControl = new FormControl(new Date().toISOString());
  reservation$: Observable<ReservationLineDaySchedule>;

  allChildrenReservedToHome = false;
  allChildrenReservedToSchool = false;

  checkedToSchool = false;
  checkedToHome = false;
  placeToSchool: string;
  placeToHome: string;
  child: Child;
  stopsToSchool: LineStopInfo[];
  stopsToHome: LineStopInfo[];
  private currentReservation: ReservationLineDaySchedule;

  constructor(
    @Inject(LOCALE_ID) private locale: string,
    private store: Store<AppState>,
    private lineService: LineService,
    private _adapter: DateAdapter<any>,
    private datePipe: DatePipe,
    public errorDialog: MatDialog,
    public dialog: MatDialog) {
      this._adapter.setLocale('it');
  }

  ngOnInit() {
    console.log('ParentReservationComponent.ngOnInit()');
    this.store.subscribe(
      s => {
        if (s.authState.user.children !== null && s.authState.user.children !== undefined && s.authState.user.children.length > 0) {
          this.childrenWithStatus = s.authState.user.children.map(value => new ChildLineScheduleStatus(value, false, ''));
          this.childrenWithStatusFiltered = s.authState.user.children.map(value => new ChildLineScheduleStatus(value, false, ''));
        }
        const roles = s.authState.user.roles;
        this.lines =  [].concat.apply([], Object.keys(roles).filter(value => value.match(UserRole.PARENT))
          .map(value => roles[value]));
      }
    );

    this.days$.subscribe(
      value => {
        this.filter = null;
        this.filter = (d: Date): boolean => {
          const formattedDay = formatDate(d, 'yyyy-MM-dd', this.locale);
          const allowedDays = value.map(c => {
            return formatDate(c, 'yyyy-MM-dd', this.locale);
          });
          const today = new Date().setHours(0, 0, 0, 0);
          return (d.getTime() >= today && allowedDays.includes(formattedDay));
        };
      });
  }

  ngAfterContentInit(): void {
    console.log('ParentReservationComponent.ngAfterContentInit()');
    this.line = this.lines.length > 0 ? this.lines[0] : null;
    this.updateLineCalendar();
  }

  updateLineCalendar() {
    console.log('ParentReservationComponent.updateLineCalendar() line: ' + this.line.toString());
    if (this.line !== null) {
     this.lineService.getLineScheduleCalendar(this.line).pipe(
        map(payload => payload.days)).
      subscribe(
        days => {
          this.days$.next(days);
          if (days.length > 0) {
            this.day = days[0];
            this.changeDayLineSchedule();
          }
        }
      );
      this.lineService.getLineDetail(this.line).pipe(
        map(payload => payload)).
      subscribe(
        (line: LineStops) => {
          this.stopsToSchool = _.initial(line.toSchool);
          this.stopsToHome = _.tail(line.toHome);
        }
      );
    }
  }

  changeDayLineSchedule() {
    const line = this.line;
    if (line !== null && this.day !== null) {
      console.log('ParentReservationComponent - get reservation:' + this.line + ' day' + this.day);
      this.reservation$ = this.lineService.getLineDayParentReservation(line, this.day).
      pipe(
        tap(value => {
          this.currentReservation = value;
          this.updateStatusChildrenReservation();
          this.filterChildren();
          this.child = null;
        }),
        catchError(err => throwError(err))
      );
    }
  }

  /* called by template*/
  addReservation() {
    let time = null;
    this.stopsToSchool.forEach(
      s => {
        if (s.name.localeCompare(this.placeToSchool) === 0) {
          time = s.time;
        }
      }
    );
    if (this.checkedToSchool) {
      const addReservationChildren = new AddReservationChildren(
        [this.child],
          this.placeToSchool,
          'TO_SCHOOL'
        );
      this.lineService.addChildrenReservation(this.line, this.day, addReservationChildren).pipe(
        catchError(err => {
          console.log(`addChildrenReservation ${err}`);
          const d = this.errorDialog.open(ErrorDialogComponent, {
            width: '250px',
            height: '200px',
            data: 'Impossibile aggiungere la prenotazione per ANDATA - Troppo tardi'
          });
          return throwError(err);
        })
      ).subscribe(
        value => this.changeDayLineSchedule()
      );
    }
    if (this.checkedToHome) {
      const addReservationChildren = new AddReservationChildren(
        [this.child],
        this.placeToHome,
        'TO_HOME'
        );
      this.lineService.addChildrenReservation(this.line, this.day, addReservationChildren).pipe(
          catchError(err => {
            console.log(`addChildrenReservation ${err}`);
            const d = this.errorDialog.open(ErrorDialogComponent, {
              width: '250px',
              height: '200px',
              data: 'Impossibile aggiungere la prenotazione per RITORNO - Troppo tardi'
            });
            return throwError(err);
          })
        )
        .subscribe(
        value => this.changeDayLineSchedule()
      );
    }

  }

  /* called by template*/
  changeLineSchedule(line: string) {
    console.log('ParentReservationComponent.changeLineSchedule() line: ' + line);
    if (this.line === line) {
      return;
    }
    this.line = line;
    this.updateLineCalendar();
  }

  /* called by template */
  changeDaySchedule() {
    this.day = this.dateFormControl.value;
    console.log('ParentReservationComponent.changeDaySchedule() day: ' + this.day.toString());
    this.changeDayLineSchedule();
  }

  /* called by template */
  updateStopToSchool(name: string) {
    this.placeToSchool = name;
  }

  /* called by template */
  updateStopToHome(name: string) {
    this.placeToHome = name;
  }

  /* called by template */
  updateSelectedChild(child: Child) {
    this.child = child;
  }

  /* called by template */
  isInputOk() {
    return this.child !== null && this.child !== undefined &&
      (
        (this.placeToSchool !== null && this.placeToSchool !== undefined && this.checkedToSchool) ||
        (this.placeToHome !== null && this.placeToHome !== undefined && this.checkedToHome)
      );
  }

  /*called by template*/
  removeReservation(child: ChildReservation) {
    this.lineService.deleteChildReservation(this.line, child.ticket).pipe(
      tap(x => {
        this.changeDayLineSchedule();
      }),
      catchError(err => throwError(err))
    ).subscribe();
  }

  /* called also by template */
  filterChildren() {
    const cR = this.currentReservation;
    this.childrenWithStatusFiltered = _.cloneDeep<ChildLineScheduleStatus[]>(this.childrenWithStatus);
    const toS = cR === null || cR === undefined || cR.toSchool === null || cR.toSchool === undefined ? null : cR.toSchool;
    const toH = cR === null || cR === undefined || cR.toHome === null || cR.toHome === undefined ? null : cR.toHome;
    const childrenStatus = cR.children;

    if (this.checkedToHome) {

      childrenStatus.forEach(_child => {
        _.remove(this.childrenWithStatusFiltered, function (currentObj) {
          return _child.name === currentObj.child.name &&
            _child.surname === currentObj.child.surname && _child.reservedToHome;
        });
      });

    }

    if (this.checkedToSchool) {

      childrenStatus.forEach(_child => {
        _.remove(this.childrenWithStatusFiltered, function (currentObj) {
          return _child.name === currentObj.child.name &&
            _child.surname === currentObj.child.surname && _child.reservedToSchool;
        });
      });

    }

   /* if (toH !== null &&
      toH !== undefined &&
      toH.length > 0 && this.checkedToHome) {
      toH.forEach(v => {
        if (v.offBoard !== null && v.offBoard !== undefined && v.offBoard.length > 0) {
          v.offBoard.forEach(_child => {
            _.remove(this.childrenWithStatusFiltered, function (currentObj) {
              return _child.name === currentObj.child.name &&
                _child.surname === currentObj.child.surname;
            });
          });
        }
      });
    }
    if (toS !== null &&
      toS !== undefined &&
      toS.length > 0 && this.checkedToSchool) {
      toS.forEach(v => {
        if (v.onBoard !== null && v.onBoard !== undefined && v.onBoard.length > 0) {
          v.onBoard.forEach(_child => {
            _.remove(this.childrenWithStatusFiltered, function (currentObj) {
              return _child.name === currentObj.child.name &&
                _child.surname === currentObj.child.surname;
            });
          });
        }
      });
    }*/
  }

  updateStatusChildrenReservation() {

    console.log('ParentReservationComponent.updateStatusChildrenReservation() - START');
    const cR = this.currentReservation;
    const toH = cR === null || cR === undefined || cR.toHome === null || cR.toHome === undefined ? null : cR.toHome;
    const toS = cR === null || cR === undefined || cR.toSchool === null || cR.toSchool === undefined ? null : cR.toSchool;

    /*
    * clean template data
    * */
    this.childrenWithStatus.forEach(cStatus => {
      cStatus.reserved = false;
    });
    this.allChildrenReservedToHome = false;
    this.allChildrenReservedToSchool = false;

    if (toH !== null &&
      toH !== undefined &&
      toH.length > 0) {
      let i = 0;
      toH.forEach(v => {
        if (v.offBoard !== null && v.offBoard !== undefined && v.offBoard.length > 0) {
          const reservations = this.childrenWithStatus.length;
          v.offBoard.forEach(_child => {
            this.childrenWithStatus.forEach(cStatus => {
              if (cStatus.child.name === _child.name && cStatus.child.surname === _child.surname) {
                cStatus.reserved = true;
                cStatus.ticket = _child.ticket;
                i++;
              }
            });
          });
          if (i === reservations) {
            this.checkedToHome = false;
            this.allChildrenReservedToHome = true;
          }
        }
      });
    }


    if (toS !== null &&
      toS !== undefined &&
      toS.length > 0) {
      let i = 0;
      toS.forEach(v => {
        if (v.onBoard !== null && v.onBoard !== undefined && v.onBoard.length > 0) {
          const reservations = this.childrenWithStatus.length;
          v.onBoard.forEach(_child => {
            this.childrenWithStatus.forEach(cStatus => {
              if (cStatus.child.name === _child.name && cStatus.child.surname === _child.surname) {
                cStatus.reserved = true;
                cStatus.ticket = _child.ticket;
                i++;
              }
            });
          });
          if (i === reservations) {
            this.checkedToSchool = false;
            this.allChildrenReservedToSchool = true;
          }
        }
      });
    }
    console.log('ParentReservationComponent.updateStatusChildrenReservation() - FINE');
  }

  isModifiable(time: string): boolean {
    if (this.day !== null) {
      const today = this.datePipe.transform(new Date(), 'dd-MM-yyyy');
      const scheduleDay = this.datePipe.transform(this.day, 'dd-MM-yyyy');

      // Data antecedente a oggi, prenotazione non più modificabile
      if (today.localeCompare(scheduleDay) > 0) {
        return false;
      } else if (today.localeCompare(scheduleDay) === 0)  {
        if (time.localeCompare(new Date().getHours().toString(10) + ':' + new Date().getMinutes().toString(10)) < 0) {
          return false;
        }
      }
    }
    return true;
  }


  openMap(stop: LineStopDetail) {
    console.log('lat lon: ' + stop.latitude, stop.longitude);
    /*const d = this.dialog.open(MapDialogComponent,  {
      width: '75%',
      height: '75%',
      data: {latitude: latitude, longitude: longitude}
    });*/
    // tslint:disable-next-line:max-line-length
    const mapsWindow = window.open('https://www.google.com/maps/search/?api=1&query=' + stop.latitude + ',' + stop.longitude, 'Google Maps');
  }
}
