import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentReservationComponent } from './parent-reservation.component';

describe('ParentReservationComponent', () => {
  let component: ParentReservationComponent;
  let fixture: ComponentFixture<ParentReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentReservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
