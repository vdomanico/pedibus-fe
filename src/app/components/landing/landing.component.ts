import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {UserRole} from '../../model/UserRole';
import {AppState} from '../../store/app.states';
import {Store} from '@ngrx/store';
import {map} from 'rxjs/operators';
import {User} from '../../model/User';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  roles$: Observable<string[]>;
  L_ADMIN = UserRole.LINE_ADMINISTRATOR;
  CHAPERON = UserRole.CHAPERON;
  PARENT = UserRole.PARENT;
  asset = 'assets/img';

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.roles$ = this.store.select(appState => appState.authState.user).pipe(
      map( (user: User) => {
        return user.roles !== null
        && user.roles !== undefined &&
        Object.keys(user.roles).length > 0 ?  Array.from(Object.keys(user.roles)) : null;
      }));
  }

}
