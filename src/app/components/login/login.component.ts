import { AuthService } from '../../auth.service';
import { Component, OnInit } from '@angular/core';
import {FormGroup, Validators, FormControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ErrorDialogService} from '../../error/error-dialog.service';
import {Credentials} from '../../model/Credentials';
import {AppState} from '../../store/app.states';
import {Store} from '@ngrx/store';
import {AuthActionTypes} from '../../store/actions/auth.actions';
import {catchError, tap} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  notFocused = false;
  form: FormGroup;
  message: string;
  credentials: Credentials = { email: null, password: null};
  processing = false;

  constructor(private authService: AuthService,
              private router: Router,
              private errorDialogService: ErrorDialogService,
              private route: ActivatedRoute,
              private store: Store<AppState>
              ) {
    this.route.queryParams.subscribe(params => {
      this.message = params['message'];
    });
    this.authService.skipIfAlreadyLoggedIn();
  }

  ngOnInit() {
    this.form = new FormGroup({
      'email': new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      'password': new FormControl('', [
        Validators.required
      ])
    });
  }

  onLogin() {
    const val = this.form.value;
    if (!this.form.invalid) {
      this.processing = true;
      this.credentials.email = val.email;
      this.credentials.password = val.password;
      console.log('LoginComponent.onLogin()' + this.credentials.email);
      this.authService.login(this.credentials).pipe(
        tap(payload => {
          this.processing = false;
          location.reload();
          this.store.dispatch({type: AuthActionTypes.LOGIN, payload: payload});
        }),
        catchError((error) => {
          this.processing = false;
          return error;
        })
      ).subscribe();
    } else {
      console.log('LoginComponent.onLogin() form is invalid ');
    }
  }
}
