import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {UserManagement} from '../../model/UserManagement';
import {MAT_DIALOG_DATA, MatTableDataSource} from '@angular/material';
import {UserRole} from '../../model/UserRole';
import {LineR} from '../../model/LineR';
import {updateUser} from '../../store/actions/user.actions';
import {AddRemoveGrant} from '../../model/AddRemoveGrant';
import {UserService} from '../../services/user/user.service';
import {catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/app.states';
import {mergeUser} from '../../store/actions/auth.actions';
import * as _ from 'lodash';

@Component({
  selector: 'app-user-grants',
  templateUrl: './user-grants.component.html',
  styleUrls: ['./user-grants.component.css']
})
export class UserGrantsComponent implements OnInit, OnDestroy {

  candidate;
  columnsToDisplay = ['line', 'line_administrator', 'chaperon', 'parent'];
  dataSource = new MatTableDataSource<LineR>();
  mySelf;
  store$;

  constructor(@Inject(MAT_DIALOG_DATA) private userM: UserManagement,
              private store: Store<AppState>,
              private userService: UserService) {
    this.candidate = userM;
  }

  ngOnInit() {
   this.store$ = this.store.subscribe(value => {
      console.log('UserGrantsComponent.subscribe users ngOnInit');
      this.mySelf = value.authState.user.email === this.userM.email;
      this.dataSource.data = this.getAddRemoveLinesGrant(this.candidate.roles, value.authState.user.roles);
    });
  }

  private addRemoveGrant(line: LineR, userRole: UserRole, add: boolean) {

    console.log('addRemoveGrant update local object - start');
    if (this.candidate.roles === null || this.candidate.roles === undefined) {
      this.candidate.roles = {};
    }

    if (add) {
      const ix = Object.keys(this.candidate.roles).indexOf(userRole, -1);
      if ( ix > -1) {
        line.roles.push(userRole);
        Object.keys(this.candidate.roles).map((key: UserRole) => {
          if (key.match(userRole)) {
            const _lines = this.candidate.roles[key];
            if (_lines == null ) {
              this.candidate.roles[key] = [line.line];
            } else {
              this.candidate.roles[key].push(line.line);
            }
          }
        });
      } else {
        if (this.candidate.roles[userRole] == null || this.candidate.roles[userRole] === undefined) {
          this.candidate.roles[userRole] = [line.line];
        } else {
          this.candidate.roles[userRole].push(line.line);
        }
      }
    } else {
      if (line.roles !== null && line.roles !== undefined) {
        _.pull(line.roles, userRole);
      }
      Object.keys(this.candidate.roles).forEach( k => {
        if (k.match(userRole)) {
          const _lines = this.candidate.roles[k];
          if (_lines !== null && _lines !== undefined) {
            _.pull(_lines, line.line);
            // if no lines exists for the role than remove the role also
            if (_.isEmpty(_lines)) {
              delete this.candidate.roles[k];
            }
          }
        }
      });
    }
    if (this.mySelf) {
      console.log(this.candidate);
      this.store.dispatch(mergeUser({payload: this.candidate}));
    } else {
      this.store.dispatch(updateUser({payload: this.candidate}));
    }
  }

  /* here i generate an object with lines of the current user loggedIn
    and the grants of the user selected for the loggedIn user lines
  */

  getAddRemoveLinesGrant(userAddRemove: Map<UserRole, string[]>, userLogged: {role: UserRole, lines: string[]}): LineR[] {
      const lines: LineR[] = [];
      console.log('getAddRemoveLinesGrant - start ' + JSON.stringify(userAddRemove));

      // i collect the lines where the user has line administration grants
      const distinctLines: Set<string> = new Set<string>();
      Object.keys(userLogged).filter((role: UserRole) => role.match(UserRole.LINE_ADMINISTRATOR)).forEach((userR: UserRole) => {
        Array.from(userLogged[userR]).forEach( l => distinctLines.add(l.toString()));
      });
      // i collect the lines where the user has a role
      const distinctMyLines: Set<string> = new Set<string>();
      Object.keys(userLogged).forEach((userR: UserRole) => {
        Array.from(userLogged[userR]).forEach( l => distinctMyLines.add(l.toString()));
      });
      // i check which grants the candidate user have
      if (this.mySelf) {
        distinctMyLines.forEach( (el: string) => {
          const line = new LineR();
          line.line = el.toString();
          line.roles = [];
          Object.keys(userAddRemove).forEach(
            (value: UserRole) => {
              const _l: Array<string> = userAddRemove[value];
              if (_l.includes(el.toString())) {
                line.roles.push(value);
              }
            }
          );
          lines.push(line);
        });
      } else {
        distinctLines.forEach( (el: string) => {
        const line = new LineR();
        line.line = el.toString();
        line.roles = [];
        Object.keys(userAddRemove).forEach(
          (value: UserRole) => {
            const _l: Array<string> = userAddRemove[value];
            if (_l.includes(el.toString())) {
              line.roles.push(value);
            }
          }
        );
        lines.push(line);
      });
      }

      console.log('getAddRemoveLinesGrant - end' + lines.toString());
      //
      lines.sort((l1, l2) => l1.line.localeCompare(l2.line));
      return lines;
    }

  isLineAdministrator(line: LineR) {
    return line.roles.includes(UserRole.LINE_ADMINISTRATOR);
  }

  isChaperon(line: LineR) {
    return line.roles.includes(UserRole.CHAPERON);
  }

  isParent(line: LineR) {
    return line.roles.includes(UserRole.PARENT);
  }

  enableDisableLineAdministrator(line: LineR) {
    this.userService.addRemoveUserGrant(
      new AddRemoveGrant(line.line.toString(), !this.isLineAdministrator(line), this.candidate.email, UserRole.LINE_ADMINISTRATOR))
      .pipe(
        catchError(err => {
          console.log(`enableDisableLineAdministrator ${err}`);
          return throwError(err);
        })
      ).subscribe(() => {
      this.addRemoveGrant(line, UserRole.LINE_ADMINISTRATOR, !this.isLineAdministrator(line));
    });
  }

  enableDisableChaperon(line) {
    this.userService.addRemoveUserGrant(
      new AddRemoveGrant(line.line.toString(), !this.isChaperon(line), this.candidate.email, UserRole.CHAPERON))
      .pipe(
        catchError(err => {
          console.log(`enableDisableLineChaperon ${err}`);
          return throwError(err);
        })
      ).subscribe(() => {
      this.addRemoveGrant(line, UserRole.CHAPERON, !this.isChaperon(line));
    });
  }

  enableDisableParent(line) {
    this.userService.addRemoveUserGrant(
      new AddRemoveGrant(line.line.toString(), !this.isParent(line), this.candidate.email, UserRole.PARENT))
      .pipe(
        catchError(err => {
          console.log(`enableDisableParent ${err}`);
          return throwError(err);
        })
      ).subscribe( () => {
      this.addRemoveGrant(line, UserRole.PARENT, !this.isParent(line));
    });
  }

  ngOnDestroy() {
    this.store$.unsubscribe();
  }
}
