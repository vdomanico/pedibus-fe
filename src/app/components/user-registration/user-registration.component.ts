import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/app.states';
import {RegisterUser} from '../../model/RegisterUser';
import {UserActionTypes} from '../../store/actions/user.actions';
import {UserRole} from '../../model/UserRole';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {

  userFormGroup: FormGroup;
  lines$: Observable<string[]>;
  lines: string[];
  line: string;
  roles: Array<string>;

  private user: RegisterUser = new RegisterUser();
  hideP = true;
  hideC = true;

  constructor(private _formBuilder: FormBuilder,
              private router: Router,
              private store: Store<AppState>,
              private snackBar: MatSnackBar) { }

  static validateIfEqualPassword(group: FormGroup) {
    if (group.value.passwordCtrl === group.value.confirmationPasswordCtrl) {
      return null;
    }
    return  {'equalPassword': true};
  }

  ngOnInit() {
    this.lines$ = this.getLineOptions();
    this.getLines();
    this.userFormGroup = this._formBuilder.group({
      nameCtrl: ['', [Validators.required, Validators.minLength(1)]],
      surnameCtrl: ['', [Validators.required, Validators.minLength(1)]],
      emailCtrl: ['', [Validators.required, Validators.email]],
      passwordCtrl: ['', [Validators.required, Validators.minLength(5)]],
      confirmationPasswordCtrl: ['', [Validators.required, Validators.minLength(5)]],
      roleCtrl: ['', Validators.required],
      lineCtrl: ['', Validators.required]
    }, {validator: [UserRegistrationComponent.validateIfEqualPassword]});
  }

  onSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  onRegisterUser() {
    if (!this.userFormGroup.invalid) {
      this.user.name = this.userFormGroup.value.nameCtrl;
      this.user.surname = this.userFormGroup.value.surnameCtrl;
      this.user.email = this.userFormGroup.value.emailCtrl;
      this.user.pwd = this.userFormGroup.value.passwordCtrl;
      this.user.confirmationPwd = this.userFormGroup.value.confirmationPasswordCtrl;
      this.user.line = this.line;
      this.user.roles = this.roles;
      this.store.dispatch({type: UserActionTypes.REGISTER_USER, payload: this.user});

      this.snackBar.open('Registrazione effettuata!', 'Torna alla home', {
        duration: 4000,
      }).afterDismissed().subscribe(() => {
        console.log('snackBar dismissed');
        this.router.navigate(['/landing']);
      });
    }
  }


  getRoleOptions(): string[] {
    return Array.of(UserRole.LINE_ADMINISTRATOR.toString(), UserRole.CHAPERON.toString(), UserRole.PARENT.toString());
  }

  /*
  * I can only register a new User for the lines I have LINE_ADMINISTRATOR grants
  * */
  getLineOptions(): Observable<string[]> {
      return this.store.select(state => state.authState.user.roles).pipe(
       map(rMap => Object.keys(rMap)
         .filter(value => value.match(UserRole.LINE_ADMINISTRATOR))
         .map(value => rMap[value])));
  }

  public getLines() {
    this.store.subscribe(
      s => {
        const roles = s.authState.user.roles;
        this.lines =  [].concat.apply([], Object.keys(roles).filter(value => value.match(UserRole.LINE_ADMINISTRATOR))
          .map(value => roles[value]));
        this.lines.sort();
      },
      error => {
        console.log(`lines subscribe error ${error}`);
      });
  }

  selectedRole(role: string) {
    if (!Array.isArray(this.roles) || !this.roles.length) {
      this.roles = new Array();
    }
    this.roles.push(role);
  }

  selectedLine(line: string) {
    console.log(line);
    this.line = line;
  }

}
