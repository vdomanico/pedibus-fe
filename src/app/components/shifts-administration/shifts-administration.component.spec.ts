import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftsAdministrationComponent } from './shifts-administration.component';

describe('ShiftsAdministrationComponent', () => {
  let component: ShiftsAdministrationComponent;
  let fixture: ComponentFixture<ShiftsAdministrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftsAdministrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftsAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
