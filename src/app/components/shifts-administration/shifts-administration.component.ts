import {AfterContentInit, Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import {UserRole} from '../../model/UserRole';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/app.states';
import {getLineSchedule} from '../../store/actions/schedule.actions';
import {FormControl} from '@angular/forms';
import {LineSchedule} from '../../model/LineSchedule';
import {DateAdapter, MatDialog, MatTableDataSource} from '@angular/material';
import {UserLineSchedule} from '../../model/UserLineSchedule';
import {LineService} from '../../services/line/line.service';
import {BehaviorSubject, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {formatDate} from '@angular/common';
import {AddRemoveShift} from '../../model/AddRemoveShift';
import {ErrorDialogComponent} from '../../error/error.dialog.component';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-shifts-administration',
  templateUrl: './shifts-administration.component.html',
  styleUrls: ['./shifts-administration.component.css']
})
export class ShiftsAdministrationComponent implements OnInit, AfterContentInit {

  lines: string[];
  days$: BehaviorSubject<Date[]> = new BehaviorSubject<Date[]>([]);
  schedule: LineSchedule;
  dateFormControl = new FormControl(new Date().toISOString());
  filter;
  selectedUser: UserLineSchedule;

  line: string;
  day: Date;
  columnsToDisplay = ['email', 'user', 'available', 'shift', 'busy'];
  dataSource: MatTableDataSource<UserLineSchedule>;

  constructor(
    @Inject(LOCALE_ID) private locale: string,
    private store: Store<AppState>,
    public errorDialog: MatDialog,
    private lineService: LineService,
    private _adapter: DateAdapter<any>) {
    this.dataSource = new MatTableDataSource<UserLineSchedule>();
    this._adapter.setLocale('it');
  }

  ngOnInit() {
    console.log('ShiftsAdministrationComponent.ngOnInit()');
    this.store.subscribe(
      s => {
        const roles = s.authState.user.roles;
        this.lines = [].concat.apply([], Object.keys(roles).filter(value => value.match(UserRole.LINE_ADMINISTRATOR))
          .map(value => roles[value]));
        this.lines.sort();
        this.schedule = s.scheduleState.lineSchedule;
        if (this.schedule !== null && this.schedule.users !== null) {
          this.dataSource.data = this.schedule.users;
        }
      }
    );
    this.days$.subscribe(
      value => {
        this.filter = null;
        this.filter = (d: Date): boolean => {
          const formattedDay = formatDate(d, 'yyyy-MM-dd', this.locale);
          const allowedDays = value.map(c => {
            return formatDate(c, 'yyyy-MM-dd', this.locale);
          });
          const today = new Date().setHours(0, 0, 0, 0);
          return (d.getTime() >= today && allowedDays.includes(formattedDay));
        };
      });
  }

  ngAfterContentInit(): void {
    console.log('ShiftsAdministrationComponent.ngAfterContentInit()');
    this.line = this.lines.length > 0 ? this.line = this.lines[0] : null;
    this.updateLineCalendar();
  }

  updateLineCalendar() {
    console.log('ShiftsAdministrationComponent.updateLineCalendar() line: ' + this.line.toString());
    if (this.line !== null) {
      this.lineService.getLineScheduleCalendar(this.line).pipe(
        map(payload => payload.days)).
      subscribe(
          days => {
            this.days$.next(days);
            if (days.length > 0) {
              this.day = days[0];
              this.changeDayLineSchedule();
            }
          }
      );
    }
  }

  changeDayLineSchedule() {
    const line = this.line;
    const day = this.dateFormControl.value;
    this.day = day;
    if (line !== null && day !== null) {
      console.log('ShiftsAdministrationComponent - ready to dispatch, line:' + this.line + ' day' + this.day);
      this.store.dispatch(getLineSchedule({ payload: {line, day}}));
    }
  }

  /* called by template*/
  changeLineSchedule(line: string) {
    console.log('ShiftsAdministrationComponent.changeLineSchedule() line: ' + line);
    this.line = line;
    this.updateLineCalendar();
  }

  /* called by template */
  changeDaySchedule() {
    console.log('ShiftsAdministrationComponent.changeDaySchedule() day: ' + this.day.toString());
    this.changeDayLineSchedule();
  }

  addRemoveShift(email: string, to_school: boolean, add: boolean) {
    const direction = to_school ? 'TO_SCHOOL' : 'TO_HOME';
    this.lineService.addRemoveShift(
                        new AddRemoveShift(
                        this.line.toString(),
                          formatDate(this.day, 'yyyy-MM-dd', this.locale),
                        email,
                        direction,
                        add)
    ).pipe(
      catchError((errorResponse: HttpErrorResponse) => {
        if (errorResponse.status === 404) {
          this.errorDialog.open(ErrorDialogComponent, {
            width: '250px',
            height: '200px',
            data: 'Impossibile assegnare il turno - il pedibus è già passato'
          });
        } else {
          this.errorDialog.open(ErrorDialogComponent, {
            width: '250px',
            height: '200px',
            data: 'Errore temporaneo'
          });
        }
        return throwError(errorResponse);
      })
    ).
    subscribe(
      data => {
        if (to_school) {
          this.selectedUser.to_school.shift = add;
        } else {
          this.selectedUser.to_home.shift = add;
        }
      }
    );
  }
}
