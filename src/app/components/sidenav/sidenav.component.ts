import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, OnDestroy, Renderer2} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {AuthActionTypes} from '../../store/actions/auth.actions';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/app.states';
import {Observable} from 'rxjs';
import {User} from '../../model/User';

/** @title Responsive sidenav */
@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css'],
})
export class SidenavComponent implements OnDestroy, AfterViewInit {
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  user$: Observable<User>;
  @Input() back: string;
  unreadMessages$: Observable<number>;

  constructor(changeDetectorRef: ChangeDetectorRef,
              media: MediaMatcher,
              private router: Router,
              private store: Store<AppState>,
              private elementRef: ElementRef,
              private renderer: Renderer2) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener('change', this._mobileQueryListener);
    this.user$ = this.store.select(state => state.authState.user);
    this.unreadMessages$ = this.store.select(appState => appState.messageCountState.unread);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
  }

  public onLogout() {
    console.log('onLogout()');
    this.store.dispatch({type: AuthActionTypes.LOGOUT});
  }

  goBack() {
    this.router.navigateByUrl('/landing');
  }

  public ngAfterViewInit(): void {
      const listItems = this.elementRef.nativeElement.querySelectorAll('.mat-list-item-content') as HTMLElement[];
      listItems.forEach(listItem => {
        this.renderer.setStyle(listItem, 'padding', '0px');
    });
  }


  goToCommunications() {
    this.router.navigateByUrl('/communications');
  }
}
