import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineReservationsComponent } from './line-reservations.component';

describe('LineReservationsComponent', () => {
  let component: LineReservationsComponent;
  let fixture: ComponentFixture<LineReservationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineReservationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineReservationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
