import {AfterContentInit, Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import {DateAdapter, MatIconRegistry, MatSnackBar} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import {LineService} from '../../services/line/line.service';
import {BehaviorSubject} from 'rxjs';
import {formatDate} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl} from '@angular/forms';
import {UserRole} from '../../model/UserRole';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/app.states';
import {ReservationLineDaySchedule} from '../../model/ReservationLineDaySchedule';
import {ChildReservation} from '../../model/ChildReservation';
import {ChildCollect} from '../../model/ChildCollect';
import {ShiftSchedule} from '../../model/ShiftSchedule';
import {LineStopDetail} from '../../model/LineStopDetail';
import {ChildDeliver} from '../../model/ChildDeliver';

@Component({
  selector: 'app-line-reservations',
  templateUrl: './line-reservations.component.html',
  styleUrls: ['./line-reservations.component.css']
})
export class LineReservationsComponent implements OnInit, AfterContentInit {

  dateFormControl = new FormControl(new Date().toISOString());
  lineReservation: ReservationLineDaySchedule;
  shift: ShiftSchedule;
  line: string;
  shifts$: BehaviorSubject<ShiftSchedule[]> = new BehaviorSubject<ShiftSchedule[]>([]);
  shifts: ShiftSchedule[] = [];
  filter;
  lines: string[];

  constructor(private iconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer,
              private route: ActivatedRoute,
              private router: Router,
              private lineService: LineService,
              @Inject(LOCALE_ID) private locale: string,
              private store: Store<AppState>,
              private snackBarCall: MatSnackBar,
              private snackBarConfirmed: MatSnackBar,
              private _adapter: DateAdapter<any>) {
    this.iconRegistry.addSvgIcon('adjust',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/img/adjust.svg'));
    this._adapter.setLocale('it');
  }

  ngOnInit() {

    this.store.subscribe(
      s => {
        const roles = s.authState.user.roles;
        this.lines =  [].concat.apply([], Object.keys(roles).filter(value => value.match(UserRole.CHAPERON))
          .map(value => roles[value]));
        this.lines.sort();
      });

    this.shifts$.subscribe(
      value => {
        console.log('LineReservationsComponent - shift subscription');
        console.log(value.map(a => a.date));
        this.shifts = value;
        this.filter = null;
        this.filter = (d: Date): boolean => {
          const formattedDay = formatDate(d, 'yyyy-MM-dd', this.locale);
          const allowedDays = value.map(c => {
            return formatDate(c.date, 'yyyy-MM-dd', this.locale);
          });
          const today = new Date().setHours(0, 0, 0, 0);
          return (d.getTime() >= today && allowedDays.includes(formattedDay));
        };
      });

  }

  ngAfterContentInit(): void {
    console.log('LineReservationsComponent.ngAfterContentInit()');
    this.line = this.lines.length > 0 ? this.lines[0] : null;
    this.updateLineCalendar();
  }

  updateLineCalendar() {
    console.log('LineReservationsComponent.updateLineCalendar() line: ' + this.line.toString());
    if (this.line !== null) {
      this.lineService.getLineShiftsCalendar(this.line).
      subscribe(
        lineShift => {
          this.shifts$.next(lineShift.shifts);
          if (lineShift.shifts.length > 0) {
            this.shift = lineShift.shifts[0];
            this.dateFormControl.setValue(this.shift.date);
            this.changeDayLineSchedule();
          }  else {
            this.shift = null;
          }
        }
      );
    }
  }

  changeDayLineSchedule() {
    const line = this.line;
    if (line !== null && this.shift !== null) {
        this.lineService.getLineReservationDetail(this.line, this.shift.date).subscribe(
          value => this.lineReservation = value
        );
    }
  }

  onCollectChild(selectedChild: ChildReservation, collect: boolean) {
    this.lineService.collectChild(new ChildCollect(selectedChild.ticket, collect), this.line, this.shift.date)
      .subscribe(res => {
          selectedChild.collected = collect;
        },
        (err) => {
          console.log(`onClickChild-subscribe() ${err}`);
          // TODO mostrare alert ?
        });
  }


  onDeliverChild(selectedChild: ChildReservation, deliver: boolean) {
    this.lineService.deliverChild(new ChildDeliver(selectedChild.ticket, deliver), this.line, this.shift.date)
      .subscribe(res => {
          selectedChild.delivered = deliver;
        },
        (err) => {
          console.log(`onClickChild-subscribe() ${err}`);
          // TODO mostrare alert ?
        });
  }

  /*called by template*/
  onSelectChild(selectedChild: ChildReservation, toSchool: boolean) {

    const collected = selectedChild.collected;
    const delivered = selectedChild.delivered;
    const template = toSchool ? 'Bambino arrivato a scuola' : 'Bambino consegnato ad un familiare';
    if (!collected && !delivered) {
      this.onCollectChild(selectedChild, true);
      this.snackBarCall.open('Bambino presente all\'appello', 'Chiudi', {
        duration: 4000,
      }).afterDismissed().subscribe(() => {
        console.log('snackBar dismissed');
      });
    } else if (collected && !delivered) {
      this.onDeliverChild(selectedChild, true);
      this.snackBarConfirmed.open(template, 'Chiudi', {
        duration: 4000,
      }).afterDismissed().subscribe(() => {
        console.log('snackBar dismissed');
      });
    } else if (collected && delivered) {
      return;
    }

  }

  /* called by template */
  changeDaySchedule() {
    this.shift = this.shifts.find(value => {
      return formatDate(value.date, 'yyyy-MM-dd', this.locale) ===
        formatDate(this.dateFormControl.value, 'yyyy-MM-dd', this.locale);
    });
    this.changeDayLineSchedule();
  }

  /* called by template*/
  changeLineSchedule(line: string) {
    console.log('LineReservationsComponent.changeLineSchedule() line: ' + line);
    if (this.line === line) {
      return;
    }
    this.line = line;
    this.updateLineCalendar();
  }
}

