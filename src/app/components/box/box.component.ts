import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit {

  @Input() title: string;
  @Input() route: string;
  @Input() image: string;

  constructor(private router: Router) {
  }

  ngOnInit() { }

  go() {
    return this.router.navigateByUrl( `/${this.route}`);
  }

}
