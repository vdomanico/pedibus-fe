import {AfterContentInit, Component, OnInit, ViewChild} from '@angular/core';
import {MessageService} from '../../services/message/message.service';
import {Message} from '../../model/Message';
import {MatPaginator, MatSort, MatTableDataSource, PageEvent} from '@angular/material';


@Component({
  selector: 'app-communications',
  templateUrl: './communications.component.html',
  styleUrls: ['./communications.component.css']
})
export class CommunicationsComponent implements OnInit, AfterContentInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  messagesView = true;
  currentMessage: Message;
  pageSizeOptions = [5, 10, 25, 50];
  pageNumber = 0;
  pageSize = 50;
  length = 0;
  sortHeader = 'sentDateTime';
  asc: true;
  dataSource: MatTableDataSource<Message> = new MatTableDataSource<Message>(null);
  columnsToDisplay = ['id', 'subject', 'sentDateTime', 'read'];


  constructor(private messageService: MessageService) {
    this.dataSource.sortingDataAccessor = (item, property) => {
      if (property === 'sentDateTime') {
        return item.sentDateTime;
      } else {
        return item[property];
      }
    };
  }

  ngOnInit() {
  }

  ngAfterContentInit(): void {
    this.dataSource.sort = this.sort;
    this.getMessages(this.sortHeader, this.asc, this.pageNumber, this.pageSize);
  }

  getMessages(sortOrder, asc, pageNumber, pageSize) {
    this.messageService.getMessages(sortOrder, asc, pageNumber, pageSize).subscribe(
      value => {
        this.dataSource.data = value.content;
        this.length = value.totalElements;
      }
    );
  }

  onPageEvent(pageEvent: PageEvent) {
    this.pageNumber = pageEvent.pageIndex;
    this.pageSize = pageEvent.pageSize;
    this.getMessages(null, null, pageEvent.pageIndex, pageEvent.pageSize);

  }

  /*
  * called by template
  * */
  openMessage(message: Message, firstTime: boolean) {
    if (firstTime) {
      this.messageService.readMessage(message.id).subscribe(
        () => {
          this.currentMessage = message;
          message.read = true;
          this.messagesView = false;
        }
      );
    } else {
      this.currentMessage = message;
      this.messagesView = false;
    }
  }

}
