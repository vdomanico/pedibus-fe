import {Injectable, OnDestroy} from '@angular/core';
import * as SockJS from 'sockjs-client';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../../environments/environment.prod';
import {filter, first, switchMap, tap} from 'rxjs/operators';
import {over} from 'stompjs';
import {Client} from 'stompjs';
import {Message} from 'stompjs';
import {StompSubscription} from '@stomp/stompjs';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/app.states';

@Injectable({
  providedIn: 'root'
})
export class SocketClientService {

  private ws;
  private client: Client;
  private state: BehaviorSubject<SocketClientState>;

  constructor(private store: Store<AppState>) {
    this.ws = new SockJS(environment.webSocketApi);
    this.client = over(this.ws);
    this.state = new BehaviorSubject<SocketClientState>(SocketClientState.ATTEMPTING);
    this.store.select('authState').subscribe(
      value => {
        if (value.isAuthenticated &&
          value.user !== undefined &&
          value.user !== null && value.user.token !== undefined) {
          console.log('SocketClientService - user state changed to authenticated' + value.user.token);
          this.connectClient(value.user.token);
          // this.ws.addEventListener('open', () => { });
        }
    });
  }

  static jsonHandler(message: Message): any {
    return JSON.parse(message.body);
  }

  static textHandler(message: Message): string {
    return message.body;
  }

  connectClient(token: string) {

    if (!this.client.connected) {
      console.log('SocketClientService - The client is still not connected');
      this.client.connect({'X-Authorization': `Bearer ${token}`},
        (frame) => {
          console.log('SocketClientService - NOW YOU ARE CONNECTED ');
          this.state.next(SocketClientState.CONNECTED);
        },
        error => console.log('SocketClientService - Connection Error'));
    } else {
      console.log('SocketClientService - The client is already connected');
    }
  }

  onConnect(): Observable<SocketClientState> {
    return this.state.asObservable().pipe(
      tap(x => 'SocketClientService - ON CONNECT'),
      filter(state => state === SocketClientState.CONNECTED)
    );
  }

  disconnectClient() {
    console.log('SocketClientService - disconnectClient call [connected?:]' + this.client.connected);
    if (this.client.connected) {
      this.client.disconnect(() => {
        console.log('SocketClientService - DISCONNECTED');
        this.state.next(SocketClientState.ATTEMPTING);
      });
    }
  }

  subscribeOnTopic(topic: string): Observable<Message> {
    return new Observable<Message>(observer => {
      const subscription: StompSubscription = this.client.subscribe(topic, message => {
        console.log('SocketClientService - ' + topic + ' Subscription');
        observer.next(message);
      });
    });
  }

  unsubscribeFromTopic(subscriptionId) {
    this.client.unsubscribe(subscriptionId);
  }

}

export enum SocketClientState {
  ATTEMPTING, CONNECTED
}

