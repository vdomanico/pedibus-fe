import { Injectable } from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../store/app.states';
import {HttpClient} from '@angular/common/http';
import {BackendApi} from '../../backendApi';
import {RegisterUser} from '../../model/RegisterUser';
import {AddRemoveGrant} from '../../model/AddRemoveGrant';
import {UserManagement} from '../../model/UserManagement';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private store: Store<AppState>,
    private http: HttpClient) { }

  public register(registerUser: RegisterUser) {
    return this.http.post(BackendApi.REGISTER, registerUser);
  }

  public getUsers(line: string) {
    return this.http.get<UserManagement[]>(BackendApi.USERS + (line === null ? '' : ('/' + line)));
  }

  public addRemoveUserGrant(addRemoveGrant: AddRemoveGrant) {
    return this.http.put(BackendApi.ADD_REMOVE_GRANT, addRemoveGrant);
  }

  public confirmAccount(token: string) {
    return this.http.get(`${BackendApi.CONFIRM_REGISTRATION}/${token}`);
  }

}
