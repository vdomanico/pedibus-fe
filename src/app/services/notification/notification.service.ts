import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {filter, first, map, switchMap, tap} from 'rxjs/operators';
import {Frame, Message} from 'stompjs';
import {SocketClientService, SocketClientState} from '../socketclient/socket-client.service';
import {Notification} from '../../model/Notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  notifications$: BehaviorSubject<Notification> = new BehaviorSubject(null);

  private topic = '/user/queue/notifications';
  constructor(private socketClient: SocketClientService) {

    this.socketClient.onConnect()
      .subscribe( () => {
        console.log('NotificationService - connecting');
        this.socketClient.subscribeOnTopic(this.topic)
          .pipe(
            map((msg: Frame) => msg.body)).pipe(
          tap(x => this.notifications$.next(JSON.parse(x)))).subscribe();
      });
  }

  getNotifications(): Observable<Notification> {
    return this.notifications$.asObservable().pipe(
      filter(value => value !== null)
    );
  }
}
