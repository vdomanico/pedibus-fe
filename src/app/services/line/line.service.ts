import { HttpClient } from '@angular/common/http';
import {Inject, Injectable, LOCALE_ID} from '@angular/core';
import { LINE_1_RESERVATION_DETAIL, LINE_2_RESERVATION_DETAIL } from '../../mocks/mock-LineReservations';
import {LineReservationDetail} from '../../model/line-stop-reservation';
import {catchError, map} from 'rxjs/operators';
import {Observable, pipe, throwError} from 'rxjs';
import * as moment from 'moment';
import _date = moment.unitOfTime._date;
import {LineResource} from '../../model/LineResource';
import {BackendApi} from '../../backendApi';
import {LineSchedule} from '../../model/LineSchedule';
import {formatDate} from '@angular/common';
import {LineScheduleCalendar} from '../../model/LineScheduleCalendar';
import {AddRemoveGrant} from '../../model/AddRemoveGrant';
import {AddRemoveShift} from '../../model/AddRemoveShift';
import {UserLineSchedule} from '../../model/UserLineSchedule';
import {AddRemoveAvailability} from '../../model/AddRemoveAvailability';
import {ReservationLineDaySchedule} from '../../model/ReservationLineDaySchedule';
import {AddReservationChildren} from '../../model/AddReservationChildren';
import {UpdateReservationChildren} from '../../model/UpdateReservationChildren';
import {LineStops} from '../../model/LineStops';
import {LineStopDetail} from '../../model/LineStopDetail';
import {ChildCollect} from '../../model/ChildCollect';
import {ChildDeliver} from '../../model/ChildDeliver';
import {LineShiftsCalendar} from '../../model/LineShiftsCalendar';
import {ChildReservation} from '../../model/ChildReservation';

@Injectable({
  providedIn: 'root'
})
export class LineService {

  constructor(
    @Inject(LOCALE_ID) private locale: string,
    private http: HttpClient) { }


  getLines() {
    return this.http.get<LineResource>(BackendApi.LINES);
  }

  getLineReservationDetail(line: string, date: Date) {
    const dateReservations = formatDate(date, 'yyyy-MM-dd', this.locale);
    const url = `${BackendApi.LINE_DAY_RESERVATIONS}/${line}/${dateReservations}`;
    return this.http.get<ReservationLineDaySchedule>(url).pipe(
      catchError(err => {
        console.log(`getLineDetail ${err}`);
        return throwError(err);
      })
    );
  }

  getLineDetail(line: string) {
    const url = BackendApi.LINES + '/' + line;
    return this.http.get<LineStops>(url).pipe(
      map( (lineStops: LineStops)  => {
        return lineStops;
      }),
      catchError(err => {
        console.log(`getLineDetail ${err}`);
        return throwError(err);
      })
    );
  }

  collectChild(childCollect: ChildCollect, line: string, date: Date) {
    const lineScheduleDay = formatDate(date, 'yyyy-MM-dd', this.locale);
    const url = `${BackendApi.CHILD_COLLECT}/${line}/${lineScheduleDay}`;
    return this.http.post(url, childCollect, {observe: 'response'}).pipe(
      map(res => {
        if (res.status !== 200) {
          throw new Error(res.statusText);
        }
        return res;
      }),
      catchError(err => {
        console.log(`Error setChildAttendance ${err}`);
        return throwError(err);
      })
    );
  }

  deliverChild(childDeliver: ChildDeliver, line: string, date: Date) {
    const lineScheduleDay = formatDate(date, 'yyyy-MM-dd', this.locale);
    const url = `${BackendApi.CHILD_DELIVER}/${line}/${lineScheduleDay}`;
    return this.http.post(url, childDeliver, {observe: 'response'}).pipe(
      map(res => {
        if (res.status !== 200) {
          throw new Error(res.statusText);
        }
        return res;
      }),
      catchError(err => {
        console.log(`Error setChildAttendance ${err}`);
        return throwError(err);
      })
    );
  }

  getLineSchedule(line: string, day: Date) {
    const lineScheduleDay = formatDate(day, 'yyyy-MM-dd', this.locale);
    const url = `${BackendApi.LINE_SCHEDULE}/${line}/${lineScheduleDay}`;
    return this.http.get<LineSchedule>(url);
  }

  getLineChaperonSchedule(line: string, day: Date) {
    const lineScheduleDay = formatDate(day, 'yyyy-MM-dd', this.locale);
    const url = `${BackendApi.LINE_CHAPERON_SCHEDULE}/${line}/${lineScheduleDay}`;
    return this.http.get<UserLineSchedule>(url);
  }

  getLineScheduleCalendar(line: string) {
    const url = `${BackendApi.LINE_SCHEDULE_CALENDAR}/${line}`;
    return this.http.get<LineScheduleCalendar>(url);
  }

  getLineShiftsCalendar(line: string) {
    const url = `${BackendApi.LINE_SHIFTS_CALENDAR}/${line}`;
    return this.http.get<LineShiftsCalendar>(url);
  }

  public addRemoveShift(addRemoveShift: AddRemoveShift) {
    return this.http.post(BackendApi.ADD_REMOVE_SHIFT, addRemoveShift);
  }

  public addRemoveAvailability(addRemoveAvailability: AddRemoveAvailability) {
    return this.http.post(BackendApi.ADD_REMOVE_AVAILABILITY, addRemoveAvailability);
  }

  public getLineDayParentReservation(line: string, day: Date) {
    const lineScheduleDay = formatDate(day, 'yyyy-MM-dd', this.locale);
    const url = `${BackendApi.PARENT_LINE_DAY_RESERVATION}/${line}/${lineScheduleDay}`;
    return this.http.get<ReservationLineDaySchedule>(url);
  }

  public addChildrenReservation(line: string, day: Date, addReservationChildren: AddReservationChildren) {
    const lineScheduleDay = formatDate(day, 'yyyy-MM-dd', this.locale);
    const url = `${BackendApi.PARENT_ADD_RESERVATION}/${line}/${lineScheduleDay}`;
    return this.http.post(url, addReservationChildren);
  }

  public updateChildrenReservation(line: string, day: Date, updateReservationChildren: UpdateReservationChildren) {
    const lineScheduleDay = formatDate(day, 'yyyy-MM-dd', this.locale);
    const url = `${BackendApi.PARENT_ADD_RESERVATION}/${line}/${lineScheduleDay}`;
    return this.http.put(url, updateReservationChildren);
  }

  public deleteChildReservation(line: string, ticket: string) {
    const url = `${BackendApi.DELETE_CHILD_RESERVATION}/${line}/${ticket}`;
    return this.http.delete(url);
  }

}

