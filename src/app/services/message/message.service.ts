import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BackendApi} from '../../backendApi';
import {Pageable} from '../../model/Pageable';
import {Message} from '../../model/Message';
import {Page} from '../../model/Page';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient) { }

  getMessages (sortOrder, asc: boolean, pageNumber, pageSize) {
    const params = new HttpParams()
      .set('sort', sortOrder + ',' + (asc ? 'asc' : 'desc'))
      .set('page', pageNumber)
      .set('size', pageSize);
    return this.http.get<Page<Message>>(BackendApi.MESSAGES + '?' + params.toString());
  }

  readMessage (idMessage: string) {
    return this.http.get(BackendApi.MESSAGES + '/' + idMessage);
  }

}
