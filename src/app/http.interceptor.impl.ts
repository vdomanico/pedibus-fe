import { Injectable, Inject } from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import { AuthService } from './auth.service';
import {catchError, map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {AppState} from './store/app.states';
import {BackendApi} from './backendApi';

@Injectable()
export class HttpInterceptorImpl implements HttpInterceptor {

  private ROUTE_LOGIN = 'login';
  private PUBLIC_URLS = [BackendApi.LOGIN.toString(), BackendApi.CONFIRM_REGISTRATION.toString()];
  private ASSETS = 'assets/img';

  constructor(private route: Router,
              @Inject('PATH_API') private pathUrl: string) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = AuthService.getAuthToken();

    if (!this.PUBLIC_URLS.includes(req.url) && req.url.search(this.ASSETS) === -1) {

      req = req.clone({
        url : `${this.pathUrl}/${req.url}`,
        headers: req.headers.set('Authorization', `Bearer ${token}`),
        params: req.params
      });


    } else if (req.url.search(this.ASSETS) === -1) {

      req = req.clone({
        url : `${this.pathUrl}/${req.url}`,
        params: req.params
      });
    }

    console.log(`INTERCEPTOR - http ${req.method} request for url: ${req.url},` +
      ` body: ${req.body}, token: ${req.headers.get('Authorization')}`);

    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          console.log('INTERCEPTOR - response payload', event.body);
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        console.log('INTERCEPTOR - response error status', error.status);
        if (error.status === 401) {
          this.route.navigate([this.ROUTE_LOGIN]);
        }
        return throwError(error);
      }));
  }

}
