import { HttpInterceptorImpl } from './http.interceptor.impl';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { AppComponent } from './app.component';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LineReservationsComponent } from './components/line-reservations/line-reservations.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MyMaterialModule } from './material.module';
import { environment } from 'src/environments/environment.prod';
import {ErrorDialogComponent} from './error/error.dialog.component';
import {routing} from './route.config';
import {AuthGuard} from './auth.guard';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { FrontHeaderComponent } from './components/front-header/front-header.component';
import { UserReservationComponent } from './components/user-reservation/user-reservation.component';
import {AccessGuard} from './access.guard';
import { LandingComponent } from './components/landing/landing.component';
import {EffectsModule} from '@ngrx/effects';
import {AuthEffects} from './store/effects/auth.effects';
import {StoreModule} from '@ngrx/store';
import {authReducer} from './store/reducers/auth.reducer';
import { BoxComponent } from './components/box/box.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {AgmCoreModule} from '@agm/core';
import { MatProgressSpinnerModule } from '@angular/material';
import {
  MatBadgeModule,
  MatDialogModule,
  MatPaginatorIntl,
  MatSidenav,
  MatSlideToggleModule,
  MatTabsModule,
  MatTooltipModule
} from '@angular/material';
import {OverlayContainer} from '@angular/cdk/overlay';

import {
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatExpansionModule,
  MatInputModule,
  MatPaginatorModule,
  MatSelectModule,
  MatSnackBar,
  MatSidenavModule,
  MatSnackBarModule, MatSortModule,
  MatStepperModule,
  MatTableModule
} from '@angular/material';
import { UserRegistrationComponent } from './components/user-registration/user-registration.component';
import {UserEffects} from './store/effects/user-effects.service';
import { AdministrationComponent } from './components/administration/administration.component';
import { UsersControlPanelComponent } from './components/users-control-panel/users-control-panel.component';
import {userReducer} from './store/reducers/user.reducer';
import {lineReducer} from './store/reducers/line.reducer';
import {LineEffects} from './store/effects/line.effects';
import { UserGrantsComponent } from './components/user-grants/user-grants.component';
import {DialogEffects} from './store/effects/dialog.effects';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {FooterComponent} from './components/footer/footer.component';
import { ParentChildrenComponent } from './components/parent-children/parent-children.component';
import { ShiftsAdministrationComponent } from './components/shifts-administration/shifts-administration.component';
import { AvailabilitiesChaperonComponent } from './components/availabilities-chaperon/availabilities-chaperon.component';
import {ScheduleEffects} from './store/effects/schedule.effects';
import {scheduleReducer} from './store/reducers/schedule.reducer';
import {userScheduleReducer} from './store/reducers/user.schedule.reducer';
import { ParentReservationComponent } from './components/parent-reservation/parent-reservation.component';
import {SidenavComponent} from './components/sidenav/sidenav.component';
import { AccountConfirmationComponent } from './components/account-confirmation/account-confirmation.component';
import {AccountEffects} from './store/effects/account.effects';
import { ChildrenMonitorComponent } from './components/children-monitor/children-monitor.component';
import { CommunicationsComponent } from './components/communications/communications.component';
import { LineRegistrationComponent } from './components/line-registration/line-registration.component';
import { EditReservationComponent } from './components/edit-reservation/edit-reservation.component';
import {MatPaginatorIntlImpl} from './custom/MatPaginatorIntlImpl';
import { PersonalDataComponent } from './components/personal-data/personal-data.component';
import {messageCountReducer} from './store/reducers/message.count.reducer';
import {MessageCountEffects} from './store/effects/message.count.effects';
import { MapDialogComponent } from './components/map-dialog/map-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    LineReservationsComponent,
    LoginComponent,
    ErrorDialogComponent,
    ToolbarComponent,
    FrontHeaderComponent,
    UserReservationComponent,
    LandingComponent,
    BoxComponent,
    UserRegistrationComponent,
    AdministrationComponent,
    UsersControlPanelComponent,
    UserGrantsComponent,
    ParentChildrenComponent,
    ShiftsAdministrationComponent,
    AvailabilitiesChaperonComponent,
    FooterComponent,
    ParentReservationComponent,
    SidenavComponent,
    AccountConfirmationComponent,
    ChildrenMonitorComponent,
    CommunicationsComponent,
    LineRegistrationComponent,
    EditReservationComponent,
    PersonalDataComponent,
    MapDialogComponent
  ],
  imports: [
    BrowserModule,
    MatListModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatCardModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MyMaterialModule,
    MatExpansionModule,
    MatSidenavModule,
    MatStepperModule,
    MatInputModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    AgmCoreModule.forRoot({
      apiKey: ''
    }),
    EffectsModule.forRoot([AuthEffects,
      UserEffects,
      LineEffects,
      DialogEffects,
      ScheduleEffects,
      AccountEffects,
    MessageCountEffects]),
    StoreModule.forRoot(
      {
        authState: authReducer,
        userState: userReducer,
        lineState: lineReducer,
        scheduleState: scheduleReducer,
        userScheduleState: userScheduleReducer,
        messageCountState: messageCountReducer,
      }),
    routing,
    MatSortModule,
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    MatTabsModule,
    MatTooltipModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatBadgeModule,
  ],
  providers: [
    { provide: 'PATH_API', useValue: environment.restApi },
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorImpl, multi: true },
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true, direction: 'ltr'}},
    { provide: MatPaginatorIntl, useClass: MatPaginatorIntlImpl},
    AuthGuard
  ],
  bootstrap: [AppComponent],
  entryComponents: [ErrorDialogComponent, UserGrantsComponent, MapDialogComponent],
})
export class AppModule {
  constructor(overlayContainer: OverlayContainer) {
    overlayContainer.getContainerElement().classList.add('dark-theme');
  }
}
