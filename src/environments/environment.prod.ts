export const environment = {
  production: true,
  restApi : 'https://pedibus-be.herokuapp.com/api',
  webSocketApi: 'https://pedibus-be.herokuapp.com/ws'
};
